<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route["login"] = "welcome/login";
$route["logout"] = "welcome/logout";

//ADMIN
$route["admin/dashboard"] = "welcome/index";

$route["admin/master_user"] = "UserController/index";
$route["admin/master_user/add"] = "UserController/viewadd";
$route["admin/master_user/do_add"] = "UserController/doadd";
$route["admin/master_user/edit/(:any)"] = "UserController/viewedit/$1";
$route["admin/master_user/do_edit"] = "UserController/doedit";
$route["admin/master_user/delete/(:any)"] = "UserController/dodelete/$1";

$route["admin/master_kategori"] = "KategoriController/index";
$route["admin/master_kategori/add"] = "KategoriController/viewadd";
$route["admin/master_kategori/do_add"] = "KategoriController/doadd";
$route["admin/master_kategori/edit/(:any)"] = "KategoriController/viewedit/$1";
$route["admin/master_kategori/do_edit"] = "KategoriController/doedit";
$route["admin/master_kategori/delete/(:any)"] = "KategoriController/dodelete/$1";

$route["admin/master_kue"] = "KueController/index";
$route["admin/master_kue/add"] = "KueController/viewadd";
$route["admin/master_kue/do_add"] = "KueController/doadd";
$route["admin/master_kue/edit/(:any)"] = "KueController/viewedit/$1";
$route["admin/master_kue/do_edit"] = "KueController/doedit";
$route["admin/master_kue/delete/(:any)"] = "KueController/dodelete/$1";

$route["admin/master_supplier"] = "SupplierController/index";
$route["admin/master_supplier/add"] = "SupplierController/viewadd";
$route["admin/master_supplier/do_add"] = "SupplierController/doadd";
$route["admin/master_supplier/edit/(:any)"] = "SupplierController/viewedit/$1";
$route["admin/master_supplier/do_edit"] = "SupplierController/doedit";
$route["admin/master_supplier/delete/(:any)"] = "SupplierController/dodelete/$1";

$route["admin/master_setting"] = "SettingController/index";
$route["admin/master_setting/do_edit"] = "SettingController/doedit";

$route["admin/laporan/penjualan"] = "LaporanController/laporanpenjualan";
$route["admin/laporan/kue"] = "LaporanController/laporankue";
$route["admin/laporan/profit"] = "LaporanController/laporanprofit";
$route["admin/laporan/restok"] = "LaporanController/laporanrestok";
$route["admin/laporan/cancel"] = "LaporanController/laporancancel";
$route["admin/laporan/piutang"] = "LaporanController/laporanpiutang";
$route["admin/laporan/booking"] = "LaporanController/laporanbooking";


//KASIR
$route["cart"] = "SalesController/cart";
$route["addcart"] = "SalesController/addCart";
$route["removecart"] = "SalesController/removeCart";
$route["updatecart"] = "SalesController/updateCart";

$route["submit_payment"] = "SalesController/submitPayment";
$route["update_status_order"] = "SalesController/updateStatus";

$route["kasir/sales"] = "SalesController/index";
$route["kasir/invoice"] = "SalesController/invoice";

$route["kasir/history_transaction"] = "TransactionController/index";
$route["kasir/history_transaction/(:any)"] = "TransactionController/detail/$1";

$route["kasir/request_stok"] = "KueController/requestStok";
$route["kasir/do_request_stok"] = "KueController/doRequestStok";

$route["kasir/book_kue"] = "KueController/listBookKue";
$route["kasir/addoutlet"] = "KueController/outlet";
$route["kasir/outlet/do_add"] = "KueController/addOutlet";
$route["kasir/book_kue/add"] = "KueController/addBookKue";
$route["kasir/book_kue/do_add"] = "KueController/doaddBooking";
$route["kasir/book_kue/detail/(:any)"] = "KueController/detailBooking/$1";
$route["kasir/book_kue/do_lunas"] = "KueController/doLunas";

//KOKI
$route["koki/dashboard"] = "KueController/kokiDashboard";
$route["koki/get_pending_request"] = "KueController/getPendingStok";
$route["koki/done_request"] = "KueController/doneRequest";

$route["callback/xendit"] = "SalesController/callbackXendit";

//TEST
$route["test"] = "UserController/test";
$route["generaterandomtransaction"] = "TransactionController/generateRandomTransaction";
