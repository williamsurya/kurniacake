<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function redirectURL($url)
{
    echo '<script>window.location.href = "' . $url . '"</script>';
}

function showAlert($judul, $isi, $tipe = "info", $url = "")
{
    //tipe-> error, successs,info
    echo '<div style="display:none"></div>';
    echo '<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>';
    if ($url == "back") {
        //SHOW MODAL & BACK 1 PAGE
        echo '
            <script>
                swal("' . $judul . '", "' . $isi . '", "' . $tipe . '").then((value)=>{
                    window.location.href = document.referrer;
                });
            </script>';
    } else if ($url != "") {
        //SHOW MODAL & GO TO URL
        echo '
            <script>
                swal("' . $judul . '", "' . $isi . '", "' . $tipe . '").then((value)=>{
                    window.location.href = "' . $url . '";
                });
            </script>';
    } else {
        //DEFAULT HANYA SHOW MODAL
        echo '
            <script>
                swal("' . $judul . '", "' . $isi . '", "' . $tipe . '");
            </script>';
    }
}
