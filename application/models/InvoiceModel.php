<?php
class InvoiceModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getNextID()
    {
        return $this->db->query("SELECT id FROM h_invoice ORDER BY id DESC LIMIT 1;")->row();
    }

    public function insertHeader($no_inv, $email_customer, $discount, $fee_shipping, $alamat, $nama_penerima, $grand_total, $date_delivery, $id_kasir, $payment_method)
    {
        $status = 1;
        if ($alamat != "") {
            $status = 0;
        }
        if ($payment_method == "XENDIT") {
            $status = 2;
        }
        $data = array(
            'no_inv' => $no_inv,
            'email_customer' => $email_customer,
            'discount' => $discount,
            'fee_shipping' => $fee_shipping,
            'alamat' => $alamat,
            'status' => $status,
            'nama_penerima' => $nama_penerima,
            'grand_total' => $grand_total,
            'date_delivery' => $date_delivery,
            'id_kasir' => $id_kasir
        );
        $this->db->insert('h_invoice', $data);
        return $this->db->insert_id();
    }

    public function updateStatusFromOrderID($order_id, $status)
    {

        $data = array(
            "status" => $status
        );
        $this->db->where(array(
            'no_inv' => $order_id,
        ));
        $this->db->update("h_invoice", $data);
    }

    public function updateStatus($id, $status)
    {
        $data = array(
            "status" => $status
        );
        $this->db->where(array(
            'id' => $id,
        ));
        $this->db->update("h_invoice", $data);
    }

    public function insertDetail($id_header, $id_kue, $nama_kue, $qty, $price, $subtotal)
    {
        $data = array(
            'id_header' => $id_header,
            "id_kue" => $id_kue,
            "nama_kue" => $nama_kue,
            'qty' => $qty,
            "price" => $price,
            "subtotal" => $subtotal
        );
        $this->db->insert("d_invoice", $data);
    }


    public function updateGrandTotal($id_header, $grand_total)
    {

        $data = array(
            "grand_total" => $grand_total
        );
        $this->db->where(array(
            'id' => $id_header,
        ));
        $this->db->update("h_invoice", $data);
    }

    public function updateDate($id_header, $date)
    {

        $data = array(
            "date" => $date
        );
        $this->db->where(array(
            'id' => $id_header,
        ));
        $this->db->update("h_invoice", $data);
    }

    public function getAll()
    {
        $this->db->select("*");
        $this->db->from("h_invoice");
        return $this->db->get()->result();
    }

    public function getAllHeader($id_kasir)
    {
        $this->db->select("*");
        $this->db->from("h_invoice");
        $this->db->where("id_kasir", $id_kasir);
        return $this->db->get()->result();
    }

    public function getHeader($id)
    {
        $this->db->select("*");
        $this->db->where("id", $id);
        $this->db->from("h_invoice");
        return $this->db->get()->row();
    }

    public function getDetail($id)
    {
        $this->db->select("*");
        $this->db->where("id_header", $id);
        $this->db->from("d_invoice");
        return $this->db->get()->result();
    }

    public function getPenjualanHariini()
    {
        $date = new DateTime("now");

        $curr_date = $date->format('Y-m-d');

        $this->db->select('*');
        $this->db->from('h_invoice');
        $this->db->where('DATE(date)', $curr_date); //use date function
        $query = $this->db->get();
        return $query->result();
    }

    public function getLaporanPenjualanBulanan()
    {
        $date = new DateTime("now");
        $month = $date->format('Y-m');

        $this->db->select("date,SUM(grand_total) as total");
        $this->db->from("h_invoice");
        $this->db->where("DATE_FORMAT(date,'%Y-%m')", $month);
        $this->db->group_by("DATE(date)");
        $query = $this->db->get();
        return $query->result();
    }

    public function getLaporanProfit()
    {
        $this->db->select("h_invoice.date, h_invoice.id as id_header, d_invoice.qty, kue.nama, (kue.harga_jual - kue.harga_beli) as profit");
        $this->db->from("d_invoice");
        $this->db->join("kue", "kue.id = d_invoice.id_kue");
        $this->db->join("h_invoice", "h_invoice.id = d_invoice.id_header");
        return $this->db->get()->result();
    }

    public function getLaporanPenjualanPerKasir()
    {
        $this->db->select("h_invoice.date, user.id as id_user, user.name, h_invoice.id as id_header, d_invoice.qty, kue.harga_jual, (kue.harga_jual - kue.harga_beli) as profit");
        $this->db->from("d_invoice");
        $this->db->join("kue", "kue.id = d_invoice.id_kue");
        $this->db->join("h_invoice", "h_invoice.id = d_invoice.id_header");
        $this->db->join("user", "user.id = h_invoice.id_kasir");
        return $this->db->get()->result();
    }

    public function getLaporanCancelOrder()
    {
        $this->db->select("*");
        $this->db->from("h_invoice");
        $this->db->where("status", -1);
        return $this->db->get()->result();
    }

    public function getLaporanPengeluaran()
    {
        $date = new DateTime("now");
        $month = $date->format('Y-m-d');

        $this->db->select("request_stok.qty, kue.nama, (kue.harga_beli * request_stok.qty) as total_pengeluaran");
        $this->db->from("request_stok");
        $this->db->join("kue", "kue.id = request_stok.id_kue");
        $this->db->where("DATE(request_stok.tanggal_produksi)", $month);
        return $this->db->get()->result();
    }

    public function getFilter($month, $year, $kategori)
    {
        $this->db->select("*");
        $this->db->from("h_invoice");
        $this->db->where("MONTH(DATE(date))", $month);
        $this->db->where("YEAR(DATE(date))", $year);

        if ($kategori == 'dine_in') {
            $this->db->where("alamat", "");
        } else if ($kategori == "delivery") {
            $this->db->where("alamat !=", "");
        }

        return $this->db->get()->result();
    }
}
