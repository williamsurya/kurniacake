<?php
class BookingModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getAllHeader()
    {
        $this->db->select("h_book_order.*, outlet.nama_outlet as nama_outlet, SUM(d_book_order.qty) as total_kue");
        $this->db->from("h_book_order");
        $this->db->join("d_book_order", "d_book_order.id_header = h_book_order.id");
        $this->db->join("outlet", "outlet.id = h_book_order.id_outlet");
        $this->db->group_by("d_book_order.id_header");
        return $this->db->get()->result();
    }

    public function addOutlet($nama_outlet, $alamat, $telp)
    {
        $data = array(
            'nama_outlet' => $nama_outlet,
            'alamat' => $alamat,
            'telp' => $telp
        );
        $this->db->insert('outlet', $data);
    }

    public function getOutlet()
    {
        $this->db->select("*");
        $this->db->from("outlet");
        return $this->db->get()->result();
    }

    public function addDetailBooking($id_header, $id_kue, $nama_kue, $qty, $price)
    {
        $data = array(
            'id_header' => $id_header,
            'id_kue' => $id_kue,
            'nama_kue' => $nama_kue,
            "qty" => $qty,
            "price" => $price,
            "subtotal" => $qty * $price
        );
        $this->db->insert('d_book_order', $data);
    }

    public function addHeaderBooking($id_outlet, $tanggal)
    {
        $data = array(
            'id_outlet' => $id_outlet,
            'tanggal' => $tanggal
        );
        $this->db->insert("h_book_order", $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function updateGrandTotal($id_header, $grand_total)
    {
        $data = array("grand_total" => $grand_total);

        $this->db->where(array(
            'id' => $id_header,
        ));
        $this->db->update("h_book_order", $data);
    }

    public function headerBooking($id)
    {
        $this->db->select("h_book_order.*, outlet.nama_outlet as nama_outlet, SUM(d_book_order.qty) as total_kue");
        $this->db->from("h_book_order");
        $this->db->join("d_book_order", "d_book_order.id_header = h_book_order.id");
        $this->db->join("outlet", "outlet.id = h_book_order.id_outlet");
        $this->db->where("h_book_order.id", $id);
        $this->db->group_by("d_book_order.id_header");
        return $this->db->get()->row();
    }

    public function detailBooking($id)
    {
        $this->db->select("*");
        $this->db->from("d_book_order");
        $this->db->where("id_header", $id);
        return $this->db->get()->result();
    }

    public function updateStatus($id, $status)
    {
        $data = array("status" => $status);
        $this->db->where(array(
            "id" => $id
        ));
        $this->db->update("h_book_order", $data);
    }

    public function laporanPiutang()
    {
        $this->db->select("h_book_order.*,outlet.nama_outlet as nama_outlet, SUM(d_book_order.qty) as total_kue, SUM(h_book_order.grand_total) as total_hutang");
        $this->db->join("d_book_order", "d_book_order.id_header = h_book_order.id");
        $this->db->join("outlet", "outlet.id = h_book_order.id_outlet");
        $this->db->from("h_book_order");
        $this->db->where("h_book_order.status", 0);
        $this->db->group_by("h_book_order.id_outlet");
        return $this->db->get()->result();
    }

    public function laporanBooking(){
        
        $this->db->select("h_book_order.*, outlet.nama_outlet as nama_outlet, SUM(d_book_order.qty) as total_kue");
        $this->db->from("h_book_order");
        $this->db->join("d_book_order", "d_book_order.id_header = h_book_order.id");
        $this->db->join("outlet", "outlet.id = h_book_order.id_outlet");
        $this->db->group_by("d_book_order.id_header");
        return $this->db->get()->result();
    }
}
