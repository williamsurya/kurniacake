<?php
class KategoriModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert($nama)
    {
        $data = array(
            'nama' => $nama
        );
        $this->db->insert('kategori', $data);
    }

    public function update($id, $nama)
    {
        $data = array(
            "nama" => $nama
        );
        $this->db->where(array(
            'id' => $id,
        ));
        $this->db->update("kategori", $data);
    }

    public function getAll()
    {
        $this->db->select("*");
        $this->db->from("kategori");
        $this->db->where("is_delete", 0);
        return $this->db->get()->result();
    }

    public function getSpecific($id)
    {
        $this->db->select("*");
        $this->db->where("id", $id);
        $this->db->from("kategori");
        return $this->db->get()->row();
    }
    
    public function dodelete($id)
    {
        $data = array("is_delete" => 1);
        $this->db->where(array("id" => $id));
        $this->db->update("kategori", $data);
    }
}
