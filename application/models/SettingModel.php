<?php
class SettingModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function update($id, $value)
    {
        $data = array(
            'value' => $value
        );
        $this->db->where(array(
            'id' => $id,
        ));
        $this->db->update("setting", $data);
    }

    public function getAll()
    {
        $this->db->select("*");
        $this->db->from("setting");
        return $this->db->get()->result();
    }

    public function getSpec($name)
    {
        $this->db->select("*");
        $this->db->where("nama", $name);
        $this->db->from("setting");
        return $this->db->get()->row();
    }
}
