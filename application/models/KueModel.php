<?php
class KueModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert($nama, $id_kategori, $harga_beli, $harga_jual, $durasi_kue_segar, $durasi_kue_diskon)
    {
        $data = array(
            'nama' => $nama,
            'id_kategori' => $id_kategori,
            'harga_beli' => $harga_beli,
            "harga_jual" => $harga_jual,
            "durasi_kue_segar" => $durasi_kue_segar,
            "durasi_kue_diskon" => $durasi_kue_diskon
        );
        $this->db->insert('kue', $data);
    }

    public function update($id, $nama, $id_kategori, $harga_beli, $harga_jual, $durasi_kue_segar, $durasi_kue_diskon, $diskon_kue_lama, $url = "")
    {
        if ($url != "") {
            $data = array(
                'nama' => $nama,
                'id_kategori' => $id_kategori,
                'harga_beli' => $harga_beli,
                "harga_jual" => $harga_jual,
                "durasi_kue_segar" => $durasi_kue_segar,
                "durasi_kue_diskon" => $durasi_kue_diskon,
                "diskon_kue_lama" => $diskon_kue_lama,
                "url" => $url
            );
        }
        else{
            $data = array(
                'nama' => $nama,
                'id_kategori' => $id_kategori,
                'harga_beli' => $harga_beli,
                "harga_jual" => $harga_jual,
                "durasi_kue_segar" => $durasi_kue_segar,
                "durasi_kue_diskon" => $durasi_kue_diskon,
                "diskon_kue_lama" => $diskon_kue_lama,
            );
        }
        $this->db->where(array(
            'id' => $id,
        ));
        $this->db->update("kue", $data);
    }

    public function getAll()
    {
        $this->db->select("*");
        $this->db->from("kue");
        $this->db->join("kategori", "kue.id_kategori = kategori.id");
        $this->db->where("kue.is_delete", 0);
        $this->db->select("kue.*,kategori.nama as nama_kategori");
        return $this->db->get()->result();
    }

    public function getSpecific($id)
    {
        $this->db->select("*");
        $this->db->where("id", $id);
        $this->db->from("kue");
        return $this->db->get()->row();
    }

    public function updateStok($id, $qty, $type)
    {
        // $data = array("qty" => $qty);
        $now = Date("Y-m-d");
        $this->db->set("qty", '`qty`-' . $qty, false);
        if ($type == "lama") {
            $this->db->where("tanggal_kue_diskon <", $now);
            $this->db->where("tanggal_kue_rusak >=", $now);
            $this->db->where("id_kue", $id);
            $this->db->where("status", 1);
            $this->db->where("qty >", 0);
            $this->db->order_by("id");
            $this->db->limit(1);
        } else {
            $this->db->where("tanggal_kue_diskon >=", $now);
            $this->db->where("id_kue", $id);
            $this->db->where("status", 1);
            $this->db->where("qty >", 0);
            $this->db->order_by("id");
            $this->db->limit(1);
        }
        $this->db->update("request_stok");
    }

    public function requestStok($id, $qty)
    {
        $data = array(
            'id_kue' => $id,
            'qty' => $qty,
            'status' => 0
        );
        $this->db->insert('request_stok', $data);
    }

    public function getPendingRequest()
    {
        $this->db->select("*");
        $this->db->from("request_stok");
        $this->db->join("kue", "request_stok.id_kue = kue.id");
        $this->db->where("request_stok.status", 0);
        $this->db->select("kue.*,request_stok.*");
        return $this->db->get()->result();
    }

    public function getFinishedRequest()
    {
        $this->db->select("*");
        $this->db->from("request_stok");
        $this->db->join("kue", "request_stok.id_kue = kue.id");
        $this->db->where("request_stok.status", 1);
        $this->db->select("kue.*,request_stok.*");
        return $this->db->get()->result();
    }

    public function updateStatusRequest($id, $status)
    {
        $data = array(
            "status" => $status
        );
        $this->db->where(array(
            'id' => $id,
        ));
        $this->db->update("request_stok", $data);
    }

    public function getSpesificRequest($id)
    {
        $this->db->select("*");
        $this->db->where("id", $id);
        $this->db->from("request_stok");
        return $this->db->get()->row();
    }

    public function getLaporanSold()
    {
        $this->db->select("nama_kue, sum(qty) as total_terjual");
        $this->db->from("d_invoice");
        $this->db->group_by("id_kue");
        $this->db->order_by("total_terjual", "desc");
        return $this->db->get()->result();
    }

    public function getLaporanRestok()
    {
        $this->db->select("*");
        $this->db->from("request_stok");
        $this->db->join("kue", "kue.id = request_stok.id_kue");
        return $this->db->get()->result();
    }

    public function dodelete($id)
    {
        $data = array("is_delete" => 1);
        $this->db->where(array("id" => $id));
        $this->db->update("kue", $data);
    }

    public function getStokKueSegar($id_kue)
    {
        $now = Date("Y-m-d");
        $this->db->select("sum(qty) as qty");
        $this->db->from("request_stok");
        $this->db->where("tanggal_kue_diskon >=", $now);
        $this->db->where("id_kue", $id_kue);
        $this->db->where("status", 1);
        $this->db->group_by("id_kue");
        return $this->db->get()->row();
    }

    public function getStokKueDiskon($id_kue)
    {
        $now = Date("Y-m-d");
        $this->db->select("sum(qty) as qty");
        $this->db->from("request_stok");
        $this->db->where("tanggal_kue_diskon <", $now);
        $this->db->where("tanggal_kue_rusak >=", $now);
        $this->db->where("id_kue", $id_kue);
        $this->db->where("status", 1);
        $this->db->group_by("id_kue");
        return $this->db->get()->row();
    }
}
