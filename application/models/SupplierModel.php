<?php
class supplierModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function addOutlet($nama_outlet, $alamat, $telp)
    {
        $data = array(
            'nama_outlet' => $nama_outlet,
            'alamat' => $alamat,
            'telp' => $telp
        );
        $this->db->insert('outlet', $data);
    }

    public function update($id, $nama_outlet, $telp, $alamat)
    {
        $data = array(
            'nama_outlet' => $nama_outlet,
            'alamat' => $alamat,
            'telp' => $telp
        );
        $this->db->where(array(
            'id' => $id,
        ));
        $this->db->update("outlet", $data);
    }

    public function getAll()
    {
        $this->db->select("*");
        $this->db->from("outlet");
        $this->db->where("is_delete",0);
        return $this->db->get()->result();
    }

    public function getSpecific($id)
    {
        $this->db->select("*");
        $this->db->where("id", $id);
        $this->db->from("outlet");
        return $this->db->get()->row();
    }

    public function dodelete($id)
    {
        $data = array("is_delete" => 1);
        $this->db->where(array("id" => $id));
        $this->db->update("outlet", $data);
    }
}
