<?php
class UserModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert($username, $password, $name, $role)
    {
        $data = array(
            'username' => $username,
            'password' => $password,
            'name' => $name,
            'role' => $role
        );
        $this->db->insert('user', $data);
    }

    public function update($id, $username, $name, $role)
    {
        $data = array(
            "username" => $username,
            "name" => $name,
            "role" => $role
        );
        $this->db->where(array(
            'id' => $id,
        ));
        $this->db->update("user", $data);
    }

    public function getAll()
    {
        $this->db->select("*");
        $this->db->from("user");
        $this->db->where("is_delete", 0);
        return $this->db->get()->result();
    }

    public function getSpecific($id)
    {
        $this->db->select("*");
        $this->db->where("id", $id);
        $this->db->from("user");
        return $this->db->get()->row();
    }

    public function login($username, $password)
    {
        $this->db->select("username,role,name,id");
        $this->db->where("username", $username);
        $this->db->where("password", $password);
        $this->db->where("is_delete", 0);
        $this->db->from("user");
        return $this->db->get()->row();
    }
    
    public function dodelete($id)
    {
        $data = array("is_delete" => 1);
        $this->db->where(array("id" => $id));
        $this->db->update("user", $data);
    }
}
