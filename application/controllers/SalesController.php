<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SalesController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper("CommonHelper");
        $this->load->library('session');
        $this->load->library('email');
        $this->load->model("KueModel", "Kue");
        $this->load->model("InvoiceModel", "Invoice");
        $this->load->model("SettingModel", "Setting");
    }

    public function index()
    {
        $user = $this->session->userdata("credential");
        if (isset($user)) {
            if ($user->role == 2) {
                $data["navbar"] = "Sales";
                $data["kue"] = $this->Kue->getAll();
                foreach ($data["kue"] as $dt) {
                    if ($this->Kue->getStokKueSegar($dt->id)) {
                        $dt->stok = $this->Kue->getStokKueSegar($dt->id)->qty;
                    } else {
                        $dt->stok = 0;
                    }

                    if ($this->Kue->getStokKueDiskon($dt->id)) {
                        $dt->stok_lama = $this->Kue->getStokKueDiskon($dt->id)->qty;
                    } else {
                        $dt->stok_lama = 0;
                    }
                }
                // echo json_encode($data["kue"]);
                $this->load->helper("url");
                $this->load->view("kasir/navbar", $data);
                $this->load->view("kasir/sales", $data);
                $this->load->view("kasir/footer");
            }
        } else {
            $this->session->sess_destroy();
            showAlert("Error", "please login again", "error", site_url());
        }
    }

    public function invoice()
    {
        $data["navbar"] = "Sales";
        $cart = $this->session->userdata("cart");
        $data["cart"] = [];
        $data["subtotal"] = 0;
        foreach ($cart as $index => $dt) {
            $data["cart"][$index] = $dt;
            $data["cart"][$index]->price = $this->Kue->getSpecific($dt->id_kue)->harga_jual;
            if ($dt->type == "lama") {
                $data["cart"][$index]->price = $this->Kue->getSpecific($dt->id_kue)->harga_jual - ($this->Kue->getSpecific($dt->id_kue)->harga_jual * $this->Kue->getSpecific($dt->id_kue)->diskon_kue_lama / 100);
            }
            $data["cart"][$index]->subtotal = $data["cart"][$index]->price * $dt->qty;
            $data["subtotal"] += $data["cart"][$index]->subtotal;
        }
        $data["orderid"] = "INV" . date("dmYhis");
        $data["fee_shipment_per_km"] = $this->Setting->getSpec("FEE_SHIPMENT_PER_KM");
        $data["next_id"] = $this->Invoice->getNextID()->id + 1;

        // print_r($data["fee_shipment_per_km"]);
        $this->load->helper("url");
        $this->load->view("kasir/navbar", $data);
        $this->load->view("kasir/invoice", $data);
        $this->load->view("kasir/footer");
    }


    public function submitPayment()
    {
        $user = $this->session->userdata("credential");
        $orderid = $this->input->post("orderid");
        $alamat = $this->input->post("alamat");
        $nama_penerima = $this->input->post("nama_penerima");
        $fee_shipping = $this->input->post("fee_shipping");
        $date_delivery = $this->input->post("date_delivery");
        $payment_method = $this->input->post("metode_pembayaran");
        $id_kasir = $user->id;
        // echo $user->id;
        $emailcust = $this->input->post("emailcust");

        $id_header = $this->Invoice->insertHeader($orderid, $emailcust, 0, $fee_shipping, $alamat, $nama_penerima, 0, $date_delivery, $id_kasir, $payment_method);
        $cart = $this->session->userdata("cart");
        $grand_total = $fee_shipping;
        foreach ($cart as $dt) {
            $id_kue = $dt->id_kue;
            $nama_kue = $dt->nama_kue;
            $qty = $dt->qty;
            $price = $this->Kue->getSpecific($dt->id_kue)->harga_jual;
            // $stok = $this->Kue->getSpecific($dt->id_kue)->stok;
            if ($dt->type == "lama") {
                $price = $price - ($price * $this->Kue->getSpecific($dt->id_kue)->diskon_kue_lama / 100);
                // $stok = $this->Kue->getSpecific($dt->id_kue)->stok_lama;
            }
            $subtotal = $price * $qty;
            $grand_total += $subtotal;

            // $stok -= $qty;

            $this->Invoice->insertDetail($id_header, $id_kue, $nama_kue, $qty, $price, $subtotal);
            $this->Kue->updateStok($id_kue, $qty, $dt->type);
        }
        $this->Invoice->updateGrandTotal($id_header, $grand_total);
        if ($emailcust != "") {
            $this->load->config('email');

            $from = $this->config->item('smtp_user');
            $to = $emailcust;
            $subject = "INVOICE KURNIACAKE " . $orderid;

            $cart = $this->session->userdata("cart");
            $data["cart"] = [];
            $data["subtotal"] = 0;
            $data["transaction_date"] = date("d/m/Y");
            $data["alamat"] = $alamat;
            $data["nama_penerima"] = $nama_penerima;
            $data["fee_shipping"] = $fee_shipping;
            $data["date_delivery"] = $date_delivery;
            $data["order_id"] = $orderid;
            $data["grand_total"] = $grand_total;
            foreach ($cart as $index => $dt) {
                $data["cart"][$index] = $dt;
                $data["cart"][$index]->price = $this->Kue->getSpecific($dt->id_kue)->harga_jual;
                $data["cart"][$index]->subtotal = $data["cart"][$index]->price * $dt->qty;
                $data["subtotal"] += $data["cart"][$index]->subtotal;
            }

            $this->email->set_newline("\r\n");
            $this->email->from($from);
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($this->load->view("email/invoice", $data, true));

            if ($this->email->send()) {
                echo 'Your Email has successfully been sent.';
            } else {
                show_error($this->email->print_debugger());
            }
        }

        $this->session->unset_userdata('cart');

        if ($payment_method == "MANUAL") {
            showAlert("Sukses", "Transaksi Berhasil", "success", site_url());
        } else {
            //PAYMENT WITH XENDIT

            $data = array(
                'external_id' => $orderid,
                'amount' => $grand_total
            );

            $post_data = json_encode($data);

            // Prepare new cURL resource
            $crl = curl_init('https://api.xendit.co/v2/invoices');
            curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($crl, CURLINFO_HEADER_OUT, true);
            curl_setopt($crl, CURLOPT_POST, true);
            curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

            // Set HTTP Header for POST request 
            curl_setopt(
                $crl,
                CURLOPT_HTTPHEADER,
                array(
                    'Content-Type: application/json',
                    'Authorization: Basic eG5kX2RldmVsb3BtZW50X0RtWUVvWmcwWVN0YUR1dmlldmR5Z3U5SWVla0psb2dwTmxCbXZkbGVDWXZ4MmxPT0hzYm1SWTFLTkwwNGVKOg=='
                )
            );

            // Submit the POST request
            $result = curl_exec($crl);

            $result = json_decode($result);
            if (isset($result->invoice_url)) {
                redirectURL($result->invoice_url);
            } else {
                print_r($result);
            }
            // Close cURL session handle
            curl_close($crl);
        }
    }

    public function updateStatus()
    {
        $id = $this->input->post("id");
        $status = $this->input->post("status");

        $this->Invoice->updateStatus($id, $status);
        if ($status == 1) {
            $msg = "Delivery Selesai";
        } else {
            $msg = "Order dibatalkan";
        }
        showAlert("Sukses", $msg, "success", site_url("kasir/history_transaction"));
    }

    public function cart()
    {
        $data = $this->session->userdata("cart");
        echo json_encode($data);
    }

    public function addCart()
    {
        if ($this->input->get("stok") > 0) {
            $id = $this->input->get("id_kue");
            $nama = $this->input->get("nama_kue");
            $stok = $this->input->get("stok");
            $type = $this->input->get("type");
            $data = $this->session->userdata("cart");
            if (isset($data)) {
                $index = count($data);
            } else {
                $index = null;
            }
            $exist = false;
            foreach ($data as $dt) {
                if ($dt->id_kue == $id && $dt->type == $type) {
                    if ($dt->qty < $stok) {
                        $dt->qty++;
                    }

                    $exist = true;
                    break;
                }
            }

            if (!$exist) {
                $data[$index] = new stdClass();
                $data[$index]->id_kue = $id;
                $data[$index]->nama_kue = $nama;
                if ($type == "lama") {
                    $data[$index]->nama_kue = $nama . " (Lama)";
                }
                $data[$index]->stok = $stok;
                $data[$index]->type = $type;
                $data[$index]->qty = 1;
            }
            $this->session->set_userdata("cart", $data);
        }
    }

    public function removeCart()
    {
        $index = $this->input->get("index");
        $data = $this->session->userdata("cart");
        unset($data[$index]);
        var_dump($data);
        $result = array_values($data);
        $this->session->set_userdata("cart", $result);
        // echo $index;
    }

    public function updateCart()
    {
        $index = $this->input->get("index");
        $qty = $this->input->get("qty");
        $data = $this->session->userdata("cart");
        $data[$index]->qty = $qty;
        $this->session->set_userdata("cart", $data);
    }

    public function callbackXendit()
    {
        // $order_id = $this->input->post("external_id");
        // Takes raw data from the request
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json);
        $order_id = $data->external_id;
        // echo $order_id;
        $this->Invoice->updateStatusFromOrderID($order_id, 1);
        echo $order_id;
    }
}
