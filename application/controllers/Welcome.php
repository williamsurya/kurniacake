<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper("url");
		$this->load->helper("CommonHelper");
		$this->load->library('session');
		$this->load->model("UserModel", "User");
		$this->load->model("InvoiceModel", "Invoice");
		$this->load->model("KueModel", "Kue");
	}
	public function index()
	{
		$user = $this->session->userdata("credential");
		if (isset($user)) {
			if ($user->role == 1) {
				//ADMIN PAGE
				$data["navbar"] = "Dashboard";
				$data["data_penjualan_hari_ini"] = $this->Invoice->getPenjualanHariini();
				$data["total_order"] = count($this->Invoice->getAll());
				$cake = $this->Kue->getLaporanSold();
				$data["total_cake_sold"] = 0;
				foreach ($cake as $dt) {
					$data["total_cake_sold"] += $dt->total_terjual;
				}
				$profit = $this->Invoice->getLaporanProfit();
				$data["total_profit"] = 0;
				foreach ($profit as $dt) {
					$data["total_profit"] += ($dt->qty * $dt->profit);
				}
				$restok = $this->Kue->getLaporanRestok();
				$data["total_restok"] = 0;
				foreach ($restok as $dt) {
					if ($dt->status == 0) {
						$data["total_restok"] += $dt->qty;
					}
				}
				// print_r($data_penjualan_hari_ini);
				$laporan_kasir = $this->Invoice->getLaporanPenjualanPerKasir();
				$data_laporan_per_kasir = [];
				foreach ($laporan_kasir as $dt) {
					$exist = false;
					foreach ($data_laporan_per_kasir as $d) {
						if ($d->id_user == $dt->id_user) {
							$d->profit += $dt->profit;
							$d->qty += $dt->qty;
							$d->bruto += $dt->harga_jual;
							$exist = true;
							break;
						}
					}

					if (!$exist) {
						$temp = count($data_laporan_per_kasir);
						$data_laporan_per_kasir[$temp] = new stdClass();
						$data_laporan_per_kasir[$temp]->id_user = $dt->id_user;
						$data_laporan_per_kasir[$temp]->name = $dt->name;
						$data_laporan_per_kasir[$temp]->qty = $dt->qty;
						$data_laporan_per_kasir[$temp]->profit = $dt->profit;
						$data_laporan_per_kasir[$temp]->bruto = $dt->harga_jual;
					}
				}
				$data["data_laporan_per_kasir"] = $data_laporan_per_kasir;

				$data["data_laporan_penjualan_bulanan"] = $this->Invoice->getLaporanPenjualanBulanan();
				$data["data_laporan_pengeluaran"] = $this->Invoice->getLaporanPengeluaran();

				// print_r($data["data_laporan_pengeluaran"]);

				$this->load->helper("url");
				$this->load->view("admin/navbar", $data);
				$this->load->view("admin/dashboard");
				$this->load->view("admin/footer");
			} else if ($user->role == 2) {
				//KASIR PAGE
				redirectURL(site_url("kasir/sales"));
			} else if ($user->role == 3) {
				//KOKI PAGE
				redirectURL(site_url("koki/dashboard"));
			}
		} else {
			$this->load->helper("url");
			$this->load->view('welcome_message');
		}
	}

	public function login()
	{
		$username = $this->input->post("username");
		$password = md5($this->input->post("password"));
		$data = $this->User->login($username, $password);
		if (isset($data)) {
			$this->session->set_userdata("credential", $data);
			showAlert("Success", "Login Success", "success", site_url());
		} else {
			showAlert("Login Failed", "Wrong username/password", "error", site_url());
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		showAlert("Success", "Logout Success", "success", site_url());
	}
}
