<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KueController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper("CommonHelper");
        $this->load->model("KueModel", "Kue");
        $this->load->model("KategoriModel", "Kategori");
        $this->load->model("BookingModel", "Booking");
        $this->load->library('session');
    }
    public function index()
    {
        $data["data"] = $this->Kue->getAll();
        $data["navbar"] = "Kue";

        foreach ($data["data"] as $dt) {
            if ($this->Kue->getStokKueSegar($dt->id)) {
                $dt->stok = $this->Kue->getStokKueSegar($dt->id)->qty;
            } else {
                $dt->stok = 0;
            }

            if ($this->Kue->getStokKueDiskon($dt->id)) {
                $dt->stok_lama = $this->Kue->getStokKueDiskon($dt->id)->qty;
            } else {
                $dt->stok_lama = 0;
            }
        }
        // print_r($data);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/masterkue", $data);
        $this->load->view("admin/footer");
    }

    public function viewadd()
    {
        $data["navbar"] = "Kue";
        $data["kategori"] = $this->Kategori->getAll();
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/masterkue_add");
        $this->load->view("admin/footer");
    }

    public function listBookKue()
    {
        $data["navbar"] = "Booking";
        $data["data"] =  $this->Booking->getAllHeader();
        $this->load->helper("url");
        $this->load->view("kasir/navbar", $data);
        $this->load->view("kasir/book_kue", $data);
        $this->load->view("kasir/footer");
    }

    public function doadd()
    {
        $nama = $this->input->post("nama");
        $id_kategori = $this->input->post("id_kategori");
        $harga_beli = $this->input->post("harga_beli");
        $harga_jual = $this->input->post("harga_jual");
        $durasi_kue_segar = $this->input->post("durasi_kue_segar");
        $durasi_kue_diskon = $this->input->post("durasi_kue_diskon");

        $target_dir = "uploads/kue/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $target_file = $target_dir . date("YdmHis") . "." . $imageFileType;

        // Check if image file is a actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $this->Kue->insert($nama, $id_kategori, $harga_beli, $harga_jual, $durasi_kue_segar, $durasi_kue_diskon);
                showAlert("Success", "Sukses tambah data", "success", site_url("admin/master_kue"));
            } else {
                showAlert("Error", "Error Upload Image", "error", "back");
            }
        }
    }

    public function viewedit($id)
    {
        $data["navbar"] = "Kue";
        $data["data"] = $this->Kue->getSpecific($id);
        $data["kategori"] = $this->Kategori->getAll();
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/masterkue_edit", $data);
        $this->load->view("admin/footer");
    }

    public function doedit()
    {
        $id = $this->input->post("id");
        $nama = $this->input->post("nama");
        $id_kategori = $this->input->post("id_kategori");
        $harga_beli = $this->input->post("harga_beli");
        $harga_jual = $this->input->post("harga_jual");
        $durasi_kue_segar = $this->input->post("durasi_kue_segar");
        $diskon_kue_lama = $this->input->post("diskon_kue_lama");
        $durasi_kue_diskon = $this->input->post("durasi_kue_diskon");

        $target_dir = "uploads/kue/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $target_file = $target_dir . date("YdmHis") . "." . $imageFileType;

        // Check if image file is a actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $this->Kue->update($id, $nama, $id_kategori, $harga_beli, $harga_jual, $durasi_kue_segar, $durasi_kue_diskon, $diskon_kue_lama, $target_file);
                showAlert("Success", "Sukses edit data", "success", site_url("admin/master_kue"));
            } else {
                showAlert("Error", "Error Upload Image", "error", "back");
            }
        }
    }

    public function requestStok()
    {
        $user = $this->session->userdata("credential");
        if (isset($user)) {
            if ($user->role == 2) {
                $data["navbar"] = "Stok";
                $data["data_kue"] = $this->Kue->getAll();
                $data["data_pending"] = $this->Kue->getPendingRequest();
                $data["data_finished"] = $this->Kue->getFinishedRequest();
                foreach ($data["data_pending"] as $dt) {
                    if ($this->Kue->getStokKueSegar($dt->id)) {
                        $dt->stok = $this->Kue->getStokKueSegar($dt->id)->qty;
                    } else {
                        $dt->stok = 0;
                    }

                    if ($this->Kue->getStokKueDiskon($dt->id)) {
                        $dt->stok_lama = $this->Kue->getStokKueDiskon($dt->id)->qty;
                    } else {
                        $dt->stok_lama = 0;
                    }
                }
                foreach ($data["data_kue"] as $dt) {
                    if ($this->Kue->getStokKueSegar($dt->id)) {
                        $dt->stok = $this->Kue->getStokKueSegar($dt->id)->qty;
                    } else {
                        $dt->stok = 0;
                    }

                    if ($this->Kue->getStokKueDiskon($dt->id)) {
                        $dt->stok_lama = $this->Kue->getStokKueDiskon($dt->id)->qty;
                    } else {
                        $dt->stok_lama = 0;
                    }
                }

                foreach ($data["data_finished"] as $dt) {
                    if ($this->Kue->getStokKueSegar($dt->id)) {
                        $dt->stok = $this->Kue->getStokKueSegar($dt->id)->qty;
                    } else {
                        $dt->stok = 0;
                    }

                    if ($this->Kue->getStokKueDiskon($dt->id)) {
                        $dt->stok_lama = $this->Kue->getStokKueDiskon($dt->id)->qty;
                    } else {
                        $dt->stok_lama = 0;
                    }
                }
                $this->load->helper("url");
                $this->load->view("kasir/navbar", $data);
                $this->load->view("kasir/request_stok", $data);
                $this->load->view("kasir/footer");
            }
        } else {
            $this->session->sess_destroy();
            showAlert("Error", "please login again", "error", site_url());
        }
    }

    public function doRequestStok()
    {
        $id = $this->input->post("id");
        $qty = $this->input->post("qty");

        $this->Kue->requestStok($id, $qty);
        showAlert("Sukses", "Request stok berhasil", "success", site_url("kasir/request_stok"));
    }

    public function kokiDashboard()
    {
        $user = $this->session->userdata("credential");
        if (isset($user)) {
            if ($user->role == 3) {
                $data["navbar"] = "Dashboard";
                $data["total_finished"] = count($this->Kue->getFinishedRequest());
                $this->load->helper("url");
                $this->load->view("koki/navbar", $data);
                $this->load->view("koki/dashboard", $data);
                $this->load->view("koki/footer");
            }
        } else {
            $this->session->sess_destroy();
            showAlert("Error", "please login again", "error", site_url());
        }
    }

    public function getPendingStok()
    {
        $data = $this->Kue->getPendingRequest();
        echo json_encode($data);
    }

    public function doneRequest()
    {
        $id = $this->input->post("id");

        $data_request = $this->Kue->getSpesificRequest($id);
        $data_kue = $this->Kue->getSpecific($data_request->id_kue);

        // $stok_baru = $data_kue->stok + $data_request->qty;


        $this->Kue->updateStatusRequest($id, 1);
        // $this->Kue->updateStok($data_kue->id, $stok_baru);

        showAlert("Sukses", "Restok Kue Berhasil", "success", site_url("koki/dashboard"));
    }

    public function outlet()
    {
        $data["navbar"] = "Booking";
        $this->load->helper("url");
        $this->load->view("kasir/navbar", $data);
        $this->load->view("kasir/add_outlet");
        $this->load->view("kasir/footer");
    }

    public function addOutlet()
    {
        $nama_outlet = $this->input->post("nama_outlet");
        $alamat = $this->input->post("alamat");
        $telp = $this->input->post("telp");

        $this->Booking->addOutlet($nama_outlet, $alamat, $telp);
        showAlert("Sukses", "Add Outlet berhasil", "success", site_url("kasir/book_kue"));
    }

    public function addBookKue()
    {
        $data["navbar"] = "Booking";

        $data["outlet"] = $this->Booking->getOutlet();
        $data["kue"] = $this->Kue->getAll();
        $this->load->helper("url");
        $this->load->view("kasir/navbar", $data);
        $this->load->view("kasir/add_booking");
        $this->load->view("kasir/footer");
    }

    public function doaddBooking()
    {
        $kue = explode(",", $this->input->post("data_kue"));
        $qty = explode(",", $this->input->post("data_qty"));
        $id_outlet = $this->input->post("id_outlet");
        $tanggal = $this->input->post("tanggal");

        $id_header = $this->Booking->addHeaderBooking($id_outlet, $tanggal);

        $grand_total = 0;

        for ($i = 0; $i < count($kue); $i++) {
            $data_kue = $this->Kue->getSpecific($kue[$i]);
            $price = $data_kue->harga_beli;
            $grand_total += $price * $qty[$i];
            $this->Booking->addDetailBooking($id_header, $kue[$i], $data_kue->nama, $qty[$i], $price);
        }

        $this->Booking->updateGrandTotal($id_header, $grand_total);
        showAlert("Sukses", "Add Booking Order berhasil", "success", site_url("kasir/book_kue"));
    }

    public function detailBooking($id)
    {
        $data["navbar"] = "Booking";

        $data["detail"] = $this->Booking->detailBooking($id);
        $data["header"] = $this->Booking->headerBooking($id);
        $this->load->helper("url");
        $this->load->view("kasir/navbar", $data);
        $this->load->view("kasir/detail_booking");
        $this->load->view("kasir/footer");
    }

    public function doLunas()
    {
        $id = $this->input->post("id");

        $this->Booking->updateStatus($id, 1);
        showAlert("Sukses", "Booking berhasil dilunaskan", "success", site_url("kasir/book_kue"));
    }

    public function dodelete($id)
    {
        $this->Kue->dodelete($id);
        showAlert("Success", "Sukses delete data", "success", site_url("admin/master_kue"));
    }
}
