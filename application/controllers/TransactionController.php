<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TransactionController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper("CommonHelper");
        $this->load->library('session');
        $this->load->model("KueModel", "Kue");
        $this->load->model("InvoiceModel", "Invoice");
    }

    public function index()
    {
        $user = $this->session->userdata("credential");
        if (isset($user)) {
            if ($user->role == 2) {
                $id_kasir = $user->id;
                $data["navbar"] = "History";
                $data["data"] = $this->Invoice->getAllHeader($id_kasir);
                $this->load->helper("url");
                $this->load->view("kasir/navbar", $data);
                $this->load->view("kasir/history_transaction", $data);
                $this->load->view("kasir/footer");
            }
        } else {
            $this->session->sess_destroy();
            showAlert("Error", "please login again", "error", site_url());
        }
    }

    public function detail($id)
    {
        $data["data_header"] = $this->Invoice->getHeader($id);
        $data["data_detail"] = $this->Invoice->getDetail($id);

        $data["navbar"] = "History";
        $this->load->helper("url");
        $this->load->view("kasir/navbar", $data);
        $this->load->view("kasir/history_transaction_detail", $data);
        $this->load->view("kasir/footer");
    }

    public function generateRandomTransaction()
    {
        $month = $this->input->get("month");
        $year = $this->input->get("year");
        $total_day = $this->input->get("total_day");

        for ($i = 0; $i < $total_day; $i++) {
            $date = $year . "-" . $month . "-" . ($i + 1) . " 09:00:00";
            $orderid = "INV" . date("dmYhis");
            $emailcust = "";
            $rand = round((rand(10000, 100000)) / 100);
            $grand_total = $rand * 100;
            $id_header = $this->Invoice->insertHeader($orderid, $emailcust, 0, 0, "", "", $grand_total, "", 2, "MANUAL");
            $this->Invoice->updateDate($id_header, $date);
            
            echo "Inserted transaction on date : " . $date . " with id_header : " . $id_header . " with orderid : " . $orderid . " with grand total : " . $grand_total . "<br>";
        }
    }
}
