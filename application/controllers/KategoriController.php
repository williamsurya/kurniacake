<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KategoriController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper("CommonHelper");
        $this->load->model("KategoriModel", "Kategori");
    }
    public function index()
    {
        $data["data"] = $this->Kategori->getAll();
        $data["navbar"] = "Kategori";
        // print_r($data);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/masterkategori", $data);
        $this->load->view("admin/footer");
    }

    public function viewadd()
    {
        $data["navbar"] = "Kategori";
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/masterkategori_add");
        $this->load->view("admin/footer");
    }

    public function doadd()
    {
        $nama = $this->input->post("nama");
        $this->Kategori->insert($nama);
        showAlert("Success", "Sukses tambah data", "success", site_url("admin/master_kategori"));
    }

    public function viewedit($id)
    {
        $data["navbar"] = "Kategori";
        $data["data"] = $this->Kategori->getSpecific($id);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/masterkategori_edit", $data);
        $this->load->view("admin/footer");
    }

    public function doedit()
    {
        $id = $this->input->post("id");
        $nama = $this->input->post("nama");
        $this->Kategori->update($id, $nama);
        showAlert("Success", "Sukses edit data", "success", site_url("admin/master_kategori"));
    }
    
    public function dodelete($id)
    {
        $this->Kategori->dodelete($id);
        showAlert("Success", "Sukses delete data", "success", site_url("admin/master_kategori"));
    }
}
