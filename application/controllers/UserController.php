<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper("CommonHelper");
        $this->load->model("UserModel", "User");
        $this->load->model("KueModel", "Kue");
        $this->load->library('session');
    }
    public function index()
    {
        $data["data"] = $this->User->getAll();
        $data["navbar"] = "User";
        // print_r($data);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/masteruser", $data);
        $this->load->view("admin/footer");
    }

    public function viewadd()
    {
        $data["navbar"] = "User";
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/masteruser_add");
        $this->load->view("admin/footer");
    }

    public function doadd()
    {
        $username = $this->input->post("username");
        $password = md5($this->input->post("password"));
        $role = $this->input->post("role");
        $name = $this->input->post("name");
        $this->User->insert($username, $password, $name, $role);
        showAlert("Success", "Sukses tambah data", "success", site_url("admin/master_user"));
    }

    public function viewedit($id)
    {
        $data["navbar"] = "User";
        $data["data"] = $this->User->getSpecific($id);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/masteruser_edit", $data);
        $this->load->view("admin/footer");
    }

    public function doedit()
    {
        $id = $this->input->post("id");
        $username = $this->input->post("username");
        $role = $this->input->post("role");
        $name = $this->input->post("name");
        $this->User->update($id, $username, $name, $role);
        showAlert("Success", "Sukses edit data", "success", site_url("admin/master_user"));
    }

    public function test()
    {
        $cart = $this->session->userdata("cart");
        $data["cart"] = [];
        $data["subtotal"] = 0;
        $data["transaction_date"] = date("d/m/Y");
        $data["alamat"] = "Jl Jambi 37";
        $data["nama_penerima"] = "William Surya";
        $data["fee_shipping"] = 50000;
        $data["date_delivery"] = "8/20/2021";
        $data["order_id"] = "INV2034125322";
        $data["grand_total"] = 500000;
        foreach ($cart as $index => $dt) {
            $data["cart"][$index] = $dt;
            $data["cart"][$index]->price = $this->Kue->getSpecific($dt->id_kue)->harga_jual;
            $data["cart"][$index]->subtotal = $data["cart"][$index]->price * $dt->qty;
            $data["subtotal"] += $data["cart"][$index]->subtotal;
        }

        $this->load->view("email/invoice", $data);
    }
    
    public function dodelete($id)
    {
        $this->User->dodelete($id);
        showAlert("Success", "Sukses delete data", "success", site_url("admin/master_user"));
    }
}
