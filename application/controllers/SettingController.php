<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SettingController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper("CommonHelper");
        $this->load->model("SettingModel", "Setting");
    }
    public function index()
    {
        $data["data"] = $this->Setting->getAll();
        $data["navbar"] = "Setting";
        // print_r($data);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/mastersetting", $data);
        $this->load->view("admin/footer");
    }

    public function doedit()
    {
        $id = $this->input->post("id");
        $value = $this->input->post("value");
        $this->Setting->update($id, $value);
        showAlert("Success", "Sukses edit data", "success", site_url("admin/master_setting"));
    }
}
