<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SupplierController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper("CommonHelper");
        $this->load->model("SupplierModel", "Supplier");
    }
    public function index()
    {
        $data["data"] = $this->Supplier->getAll();
        $data["navbar"] = "Supplier";
        // print_r($data);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/mastersupplier", $data);
        $this->load->view("admin/footer");
    }

    public function viewadd()
    {
        $data["navbar"] = "Supplier";
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/mastersupplier_add");
        $this->load->view("admin/footer");
    }

    public function doadd()
    {
        $nama_outlet = $this->input->post("nama_outlet");
        $alamat = $this->input->post("alamat");
        $telp = $this->input->post("telp");

        $this->Supplier->addOutlet($nama_outlet, $alamat, $telp);
        showAlert("Success", "Sukses tambah data", "success", site_url("admin/master_supplier"));
    }

    public function viewedit($id)
    {
        $data["navbar"] = "Supplier";
        $data["data"] = $this->Supplier->getSpecific($id);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/mastersupplier_edit", $data);
        $this->load->view("admin/footer");
    }

    public function doedit()
    {
        $id = $this->input->post("id");
        $nama = $this->input->post("nama_outlet");
        $telp = $this->input->post("telp");
        $alamat = $this->input->post("alamat");
        $this->Supplier->update($id, $nama, $telp, $alamat);
        showAlert("Success", "Sukses edit data", "success", site_url("admin/master_supplier"));
    }

    public function dodelete($id)
    {
        $this->Supplier->dodelete($id);
        showAlert("Success", "Sukses delete data", "success", site_url("admin/master_supplier"));
    }
}
