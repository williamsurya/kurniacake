<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LaporanController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper("CommonHelper");
        $this->load->model("KueModel", "Kue");
        $this->load->model("InvoiceModel", "Invoice");
        $this->load->model("BookingModel", "Booking");
    }

    public function laporanpenjualan()
    {
        $month = $this->input->get("month");
        $year = $this->input->get("year");
        $kategori = $this->input->get("kategori");
        if (isset($month) && isset($year) && isset($kategori)) {
            $data["data"] = $this->Invoice->getFilter($month, $year, $kategori);
        } else {
            $data["data"] = $this->Invoice->getAll();
        }
        $data["navbar"] = "laporan_penjualan";
        // print_r($data);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/laporanpenjualan", $data);
        $this->load->view("admin/footer");
    }

    public function laporankue()
    {
        $data["data"] = $this->Kue->getLaporanSold();
        $data["navbar"] = "laporan_kue";
        // print_r($data);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/laporankue", $data);
        $this->load->view("admin/footer");
    }

    public function laporanprofit()
    {

        $laporan_profit = $this->Invoice->getLaporanProfit();

        $data_profit = [];
        foreach ($laporan_profit as $dt) {
            $timestamp = strtotime($dt->date);
            $date = date("Y-m-d", $timestamp);
            $exist = false;
            foreach ($data_profit as $d) {
                if ($d->created_at == $date) {
                    $d->total_penjualan += $dt->qty;
                    $d->total_profit += $dt->profit;
                    $exist = true;
                    break;
                }
            }

            if (!$exist) {
                $temp = count($data_profit);
                $data_profit[$temp] = new stdClass();
                $data_profit[$temp]->created_at = $date;
                $data_profit[$temp]->total_penjualan = $dt->qty;
                $data_profit[$temp]->total_profit = $dt->profit;
            }
        }

        $data["data"] = $data_profit;
        $data["navbar"] = "laporan_profit";
        // echo json_encode($data);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/laporanprofit", $data);
        $this->load->view("admin/footer");
    }

    public function laporanrestok()
    {
        $laporan_restok = $this->Kue->getLaporanRestok();

        $data_restok = [];
        foreach ($laporan_restok as $dt) {
            $exist = false;
            foreach ($data_restok as $d) {
                if ($d->id_kue == $dt->id_kue) {
                    if ($dt->status == 0) {
                        $d->pending_restok += $dt->qty;
                    } else {
                        $d->total_restok += $dt->qty;
                    }
                    $exist = true;
                    break;
                }
            }

            if (!$exist) {
                $temp = count($data_restok);
                $data_restok[$temp] = new stdClass();
                $data_restok[$temp]->id_kue = $dt->id_kue;
                $data_restok[$temp]->nama = $dt->nama;
                if ($dt->status == 0) {
                    $data_restok[$temp]->pending_restok = $dt->qty;
                    $data_restok[$temp]->total_restok = 0;
                } else {
                    $data_restok[$temp]->pending_restok = 0;
                    $data_restok[$temp]->total_restok = $dt->qty;
                }
            }
        }
        $data["data"] = $data_restok;
        $data["navbar"] = "laporan_restok";
        // echo json_encode($data);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/laporanrestok", $data);
        $this->load->view("admin/footer");
    }

    public function laporancancel()
    {
        $data["data"] = $this->Invoice->getLaporanCancelOrder();
        $data["navbar"] = "laporan_cancel";
        // print_r($data);
        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/laporancancel", $data);
        $this->load->view("admin/footer");
    }

    public function laporanpiutang()
    {
        $data["data"] = $this->Booking->laporanPiutang();
        $data["navbar"] = "laporan_piutang";

        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/laporanpiutang", $data);
        $this->load->view("admin/footer");
    }

    public function laporanbooking()
    {
        $data["data"] = $this->Booking->laporanBooking();
        $data["navbar"] = "laporan_booking";

        $this->load->helper("url");
        $this->load->view("admin/navbar", $data);
        $this->load->view("admin/laporanbooking", $data);
        $this->load->view("admin/footer");
    }
}
