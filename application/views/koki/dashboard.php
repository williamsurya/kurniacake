<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v1</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3 id="total_pending">0</h3>

              <p>Pending Restock</p>
            </div>
            <div class="icon">
              <i class="ion ion-social-dropbox"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?php echo $total_finished; ?><sup style="font-size: 20px"></sup></h3>

              <p>Restock Done</p>
            </div>
            <div class="icon">
              <i class="ion ion-checkmark"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>

      <div class="row">
        <div class="col-12">

          <div class="card">
            <!-- /.card-header -->
            <div class="card-header">
              Pending Request Stok
            </div>
            <div class="card-body">
              <table id="example15" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Nama Kue</th>
                    <th>Request Penambahan</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="table-body">
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <form id="formsubmit" action="<?php echo site_url("koki/done_request"); ?>" method="POST">
        <input type="hidden" name="id" id="id">
      </form>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
  docReady(function() {
    load();
    setInterval(load, 1000);
  });

  var length = 0;

  function load() {
    $.ajax({
      url: "<?php echo site_url("/koki/get_pending_request") ?>",
      type: "GET",
      success: function(response) {
        var data = JSON.parse(response);
        // console.log(length);
        // console.log(data.length);
        if (length != data.length) {
          $("#table-body").children().remove();
          length = data.length;
          console.log(response);
          $("#total_pending").html(data.length);
          var t = $("#example15").DataTable();
          for (var i = 0; i < data.length; i++) {
            t.row.add([
              data[i].nama,
              data[i].qty,
              '<div onclick="submitRequest(' + data[i].id + ')" class="btn btn-primary">Done</div>'
            ]).draw(false);
          }
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      }
    });

  }

  function submitRequest(id) {
    $("#id").val(id);
    swal({
        title: "Selesaikan pesanan restok?",
        text: "Pastikan pesanan restok sudah selesai dan benar.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $("#formsubmit").submit();
        }
      });
  }


  function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
      // call on next available tick
      setTimeout(fn, 1);
    } else {
      document.addEventListener("DOMContentLoaded", fn);
    }
  }
</script>