<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Request Tambah Stok</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-tabs">
                        <div class="card-header p-0 pt-1">
                            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Request</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Pending</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">Done</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-one-tabContent">
                                <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Request tambah stok kue</h5>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Kue</th>
                                                        <th>Kategori Kue</th>
                                                        <th>Harga Jual</th>
                                                        <th>Stok</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($data_kue as $dt) { ?>
                                                        <tr>
                                                            <td><?php echo $dt->nama; ?></td>
                                                            <td><?php echo $dt->nama_kategori; ?></td>
                                                            <td><?php echo $dt->harga_jual; ?></td>
                                                            <td><?php echo $dt->stok; ?></td>
                                                            <td>
                                                                <a href="#" class="btn btn-primary" onclick="request(<?php echo $dt->id . ',\'' . $dt->nama . '\',\'' . $dt->stok . '\''; ?>)">Request</a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Request Pending</h5>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Kue</th>
                                                        <th>Stok</th>
                                                        <th>Request Penambahan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($data_pending as $dt) { ?>
                                                        <tr>
                                                            <td><?php echo $dt->nama; ?></td>
                                                            <td><?php echo $dt->stok; ?></td>
                                                            <td><?php echo $dt->qty; ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Finished Request</h5>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Kue</th>
                                                        <th>Request Penambahan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($data_finished as $dt) { ?>
                                                        <tr>
                                                            <td><?php echo $dt->nama; ?></td>
                                                            <td><?php echo $dt->qty; ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Request Stok</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_request" action="<?php echo site_url("/kasir/do_request_stok"); ?>" method="POST">
                    <div class="form-group">
                        <label>Nama Kue</label>
                        <p id="modal-nama">Kue Lemper</p>
                        <label>Stok</label>
                        <p id="modal-stok">5</p>
                        <label>Qty</label><br>
                        <input type="hidden" name="id" id="id_kue_request">
                        <input type="number" name="qty" id="qty_request" min="1" value="1" class="form-control form-control-sidebar">
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="submitRequest()">Request Stok</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    function request(id, nama, stok) {
        console.log(id);
        $("#id_kue_request").val(id);
        $("#modal-nama").html(nama);
        $("#modal-stok").html(stok);
        $('#modal-lg').modal('toggle');
    }

    function submitRequest() {
        $("#form_request").submit();
    }
</script>