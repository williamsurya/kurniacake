<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Transaction</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-tabs">
                        <div class="card-header p-0 pt-1">
                            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Dine in</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Delivery</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-one-tabContent">
                                <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                    <div class="card">
                                        <!-- /.card-header -->
                                        <div class="card-header">
                                            <h5>Transaksi Dine In</h5>
                                        </div>
                                        <div class="card-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>No Invoice</th>
                                                        <th>Date Transaction</th>
                                                        <th>Email Customer</th>
                                                        <th>Grand Total</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($data as $dt) {
                                                        if ($dt->alamat == "") { ?>
                                                            <tr>
                                                                <td><?php echo $dt->no_inv; ?></td>
                                                                <td><?php echo $dt->date; ?></td>
                                                                <td><?php echo $dt->email_customer; ?></td>
                                                                <td><?php echo $dt->grand_total; ?></td>
                                                                <td><a href="<?php echo site_url("kasir/history_transaction/") . $dt->id ?>" class="btn btn-primary">Detail</a></td>
                                                            </tr>
                                                    <?php }
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Delivery</h5>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>No Invoice</th>
                                                        <th>Date Transaction</th>
                                                        <th>Email Customer</th>
                                                        <th>Grand Total</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($data as $dt) {
                                                        if ($dt->alamat != "") { ?>
                                                            <tr>
                                                                <td><?php echo $dt->no_inv; ?></td>
                                                                <td><?php echo $dt->date; ?></td>
                                                                <td><?php echo $dt->email_customer; ?></td>
                                                                <td><?php echo $dt->grand_total; ?></td>
                                                                <td><a href="<?php echo site_url("kasir/history_transaction/") . $dt->id ?>" class="btn btn-primary">Detail</a></td>
                                                            </tr>
                                                    <?php }
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>