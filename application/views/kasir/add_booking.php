<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Booking Kue</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">

                    <div class="card card-primary">
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-header">
                            <h3 class="card-title">Booking Kue</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Kue</label>
                                <select class="form-control" name="kue" id="id_kue">
                                    <?php foreach ($kue as $dt) { ?>
                                        <option value="<?php echo $dt->id . "-" . $dt->harga_jual; ?>"><?php echo $dt->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Qty</label>
                                <input type="number" class="form-control" name="qty" id="qty" placeholder="Qty">
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <div class="btn btn-primary" onclick="addKue()">Add</div>
                        </div>
                    </div>

                    <div class="card card-primary">
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="form_booking" action="<?php echo site_url("/kasir/book_kue/do_add"); ?>" method="POST">
                            <input type="hidden" id="booking_kue" name="data_kue" />
                            <input type="hidden" id="booking_qty" name="data_qty" />
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Outlet</label>
                                    <select class="form-control" name="id_outlet">
                                        <?php foreach ($outlet as $dt) { ?>
                                            <option value="<?php echo $dt->id; ?>"><?php echo $dt->nama_outlet; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tanggal Booking</label>
                                    <input type="date" class="form-control" name="tanggal" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card card-primary" style="padding:15px;">
                        <!-- /.card-header -->
                        <!-- form start -->
                        <table id="table1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Kue</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th>Subtotal</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div class="btn btn-primary" onclick="submitRequest()">Submit</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    var kue = [];
    var qty = [];
    var price = [];

    function addKue() {
        var selected_kue = $("#id_kue").val();
        data = selected_kue.split("-");
        var id_kue = data[0];
        var tprice = data[1];

        var tbqty = $("#qty").val();

        var exist = false;
        kue.forEach((item, index) => {
            if (item == id_kue) {
                exist = true;
                qty[index] += parseInt(tbqty);
                $("#qty-" + id_kue).html(qty[index]);
                $("#subtotal-" + id_kue).html(qty[index] * tprice);
            }
        });
        if (!exist) {
            kue.push(id_kue);
            qty.push(parseInt(tbqty));
            price.push(tprice);
            var row = "<tr id='kue-" + id_kue + "'>" +
                "<td>" + $("#id_kue option:selected").text() + "</td>" +
                "<td id='qty-" + id_kue + "'>" + tbqty + "</td>" +
                "<td>" + tprice + "</td>" +
                "<td id='subtotal-" + id_kue + "'>" + tprice * tbqty + "</td>" +
                "<td><div class='btn btn-danger' onclick='ondelete(" + id_kue + ")'>Delete</div></td>'" +
                "</tr>";

            $("#table1 tbody").append(row);
        }

    }

    function ondelete(id_kue) {
        kue.forEach((item, index) => {
            if (item == id_kue) {
                kue.splice(index, 1);
                qty.splice(index, 1);
                price.splice(index, 1);
                $("#kue-" + id_kue).remove();
            }
        });
    }

    function submitRequest() {
        $("#booking_kue").val(kue);
        $("#booking_qty").val(qty);
        $("#form_booking").submit();
    }
</script>