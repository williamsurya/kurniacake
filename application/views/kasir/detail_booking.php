<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Booking Kue</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <div class="card card-primary">
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="form_booking" action="<?php echo site_url("/kasir/book_kue/do_add"); ?>" method="POST">
                            <input type="hidden" id="booking_kue" name="data_kue" />
                            <input type="hidden" id="booking_qty" name="data_qty" />
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Outlet</label>
                                    <input type="text" disabled value="<?php echo $header->nama_outlet; ?>" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tanggal Booking</label>
                                    <input type="date" class="form-control" name="tanggal" value="<?php echo $header->tanggal; ?>" disabled />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card card-primary" style="padding:15px;">
                        <!-- /.card-header -->
                        <!-- form start -->
                        <table id="table1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Kue</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $total = 0;
                                foreach ($detail as $d) {
                                    $total += $d->subtotal; ?>
                                    <tr>
                                        <td><?php echo $d->nama_kue; ?></td>
                                        <td><?php echo $d->qty; ?></td>
                                        <td><?php echo $d->price; ?></td>
                                        <td><?php echo number_format($d->subtotal, 0, ",", "."); ?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="3" style="text-align:center; font-weight:bold;">Grand Total</td>
                                    <td><?php echo number_format($total, 0, ",", "."); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <form action="<?php echo site_url("/kasir/book_kue/do_lunas"); ?>" method="POST">
                            <input type="hidden" name="id" value="<?php echo $header->id; ?>" />
                            <div style="margin:auto; width:250px;">
                                <input type="submit" class="btn btn-primary" style="width:250px;font-weight:bold;" value="Lunas" <?php echo $header->status == 1 ? 'disabled' : ''; ?> />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>