<style>
  .text-right {
    font-weight: bold;
    color: white;
  }

  .item-cart {
    display: flex;
    margin: 5px 0px;
  }

  .item-cart h5 {
    flex-basis: 90%;
  }

  .item-cart input {
    flex-basis: 10%;
    width: 50px;
    text-align: center;
  }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Sales</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div style="margin:25px 0;">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search" style="max-width:500px;" onkeyup="searchChange()" id="searchbar">
      </div>
      <div class="row">
        <div class="col-9">
          <div class="row">
            <?php foreach ($kue as $dt) { ?>
              <div class="col-md-4 item">
                <!-- Widget: user widget style 1 -->
                <div class="card card-widget widget-user">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header text-white" style="position:relative;background: url('<?php echo base_url($dt->url); ?>') center center;background-repeat:no-repeat;">
                    <div style="background-color:black;z-index:1;opacity:0.4;position:absolute;width:100%;height:100%;top:0px;left:0px;"></div>
                    <div style="position:absolute;z-index:5;top:15px;right:15px;">
                      <h3 class="widget-user-username text-right nama-kue" style="color:white;"><?php echo $dt->nama; ?></h3>
                      <h5 class="widget-user-desc text-right"><?php echo $dt->nama_kategori; ?></h5>
                    </div>
                  </div>
                  <div class="card-footer">
                    <div class="row">
                      <div class="col-sm-4 border-right">
                        <div class="description-block">
                          <h5 class="description-header">Rp. <?php echo number_format($dt->harga_jual, 0, ',', '.'); ?></h5>
                          <span class="description-text">HARGA</span>
                        </div>
                        <!-- /.description-block -->
                      </div>
                      <!-- /.col -->
                      <div class="col-sm-4 border-right">
                        <div class="description-block">
                          <h5 class="description-header"><?php echo $dt->stok; ?></h5>
                          <span class="description-text">STOK</span>
                        </div>
                        <!-- /.description-block -->
                      </div>
                      <!-- /.col -->
                      <div class="col-sm-4">
                        <div class="description-block">
                          <div class="btn btn-primary" <?php echo $dt->stok <= 0 ? 'disabled' : ''; ?> onclick="addCart(<?php echo $dt->id . ',\'' . $dt->nama . '\',\'' . $dt->stok . '\',\'baru\''; ?>)"><i class="fa fa-shopping-cart"></i> Add</div>
                        </div>
                        <!-- /.description-block -->
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->
                  </div>
                </div>
                <!-- /.widget-user -->
              </div>
              <?php if ($dt->stok_lama > 0) { ?>
                <!-- KUE LAMA DISKON 50% -->
                <div class="col-md-4 item">
                  <!-- Widget: user widget style 1 -->
                  <div class="card card-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header text-white" style="position:relative;background: url('<?php echo base_url($dt->url); ?>') center center;background-repeat:no-repeat;">
                      <div style="background-color:black;z-index:1;opacity:0.4;position:absolute;width:100%;height:100%;top:0px;left:0px;"></div>
                      <div style="position:absolute;z-index:5;top:15px;right:15px;">
                        <h3 class="widget-user-username text-right nama-kue" style="color:white;"><?php echo $dt->nama; ?> (Lama)</h3>
                        <h5 class="widget-user-desc text-right"><?php echo $dt->nama_kategori; ?></h5>
                      </div>
                    </div>
                    <div class="card-footer">
                      <div class="row">
                        <div class="col-sm-4 border-right">
                          <div class="description-block">
                            <h5 class="description-header">Rp. <?php echo number_format(($dt->harga_jual - ($dt->harga_jual * $dt->diskon_kue_lama / 100)), 0, ',', '.'); ?></h5>
                            <span class="description-text">HARGA</span>
                          </div>
                          <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 border-right">
                          <div class="description-block">
                            <h5 class="description-header"><?php echo $dt->stok_lama; ?></h5>
                            <span class="description-text">STOK</span>
                          </div>
                          <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4">
                          <div class="description-block">
                            <div class="btn btn-primary" <?php echo $dt->stok_lama <= 0 ? 'disabled' : ''; ?> onclick="addCart(<?php echo $dt->id . ',\'' . $dt->nama . '\',\'' . $dt->stok_lama . '\',\'lama\''; ?>)"><i class="fa fa-shopping-cart"></i> Add</div>
                          </div>
                          <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>
                  <!-- /.widget-user -->
                </div>
              <?php } ?>
            <?php } ?>
          </div>
        </div>
        <div class="col-3">
          <div class="sticky-top mb-3">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title" style="font-weight:bold;color:black;font-size:24px;">CURRENT CART</h4>
              </div>
              <div class="card-body">
                <div class="item-cart">
                  <h5 style="font-weight:bold;font-size:18px;">Nama Kue</h5>
                  <h5 style="flex-basis:10%; font-weight:bold;font-size:18px;">Qty</h5>
                </div>
                <div id="body-cart">
                </div>
              </div>
              <a href="<?php echo site_url("kasir/invoice"); ?>" class="btn btn-primary">Checkout</a>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  docReady(function() {
    loadCart();
  });

  function loadCart() {
    $("#body-cart").children().remove();
    $.ajax({
      url: "<?php echo site_url("cart") ?>",
      type: "GET",
      success: function(response) {
        var data = JSON.parse(response);
        // console.log(data.length);
        for (var i = 0; i < data.length; i++) {
          $("#body-cart").append('<div class="item-cart"><h5>' + data[i].nama_kue + '</h5><input type="number" value=' + data[i].qty + ' min=0 max=' + data[i].stok + ' onchange="qtyChange(this,' + i + ')" ></div>');
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      }
    });

  }

  function addCart(id_kue, nama, stok, type) {
    var data = new Object;
    data.id_kue = id_kue;
    data.nama_kue = nama;
    data.stok = stok;
    data.type = type;
    $.ajax({
      url: "<?php echo site_url("addcart") ?>",
      type: "GET",
      data: data,
      success: function(response) {
        loadCart();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      }
    });
  }

  function qtyChange(comp, index) {
    // console.log(index);
    if (parseInt(comp.value) > parseInt(comp.max)) {
      comp.value = comp.max;
    } else if (comp.value <= 0) {
      var data = new Object;
      data.index = index;
      $.ajax({
        url: "<?php echo site_url("removecart") ?>",
        type: "GET",
        data: data,
        success: function(response) {
          // console.log(response);
          loadCart();
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
      });
    } else {
      var data = new Object;
      data.index = index;
      data.qty = comp.value;
      $.ajax({
        url: "<?php echo site_url("updatecart") ?>",
        type: "GET",
        data: data,
        success: function(response) {
          // console.log(response);
          loadCart();
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
      });
    }
  }

  function searchChange() {
    var keyword = $("#searchbar").val();
    var listKue = $(".nama-kue");
    var item = $(".item");
    for (var i = 0; i < listKue.length; i++) {
      if ((listKue[i].innerHTML).toLowerCase().includes(keyword.toLowerCase())) {
        item[i].style.display = "block";
      } else {
        item[i].style.display = "none";
      }
    }
  }

  function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
      // call on next available tick
      setTimeout(fn, 1);
    } else {
      document.addEventListener("DOMContentLoaded", fn);
    }
  }
</script>