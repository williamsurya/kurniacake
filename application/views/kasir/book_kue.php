<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Booking Kue</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-tabs">
                        <div class="tab-content" id="custom-tabs-one-tabContent">
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="card-header">
                                    <h5>Transaksi Booking Kue</h5>
                                    <a href="<?php echo site_url("kasir/book_kue/add"); ?>">
                                        <div class="btn btn-primary">Booking Kue</div>
                                    </a>
                                </div>
                                <div class="card-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Date Transaction</th>
                                                <th>Outlet</th>
                                                <th>Total Kue</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($data as $dt) { ?>
                                                <tr>
                                                    <td><?php echo $dt->tanggal; ?></td>
                                                    <td><?php echo $dt->nama_outlet; ?></td>
                                                    <td><?php echo $dt->total_kue; ?></td>
                                                    <td><?php echo $dt->status == 0 ? 'Sedang Jalan' : 'Selesai'; ?></td>
                                                    <td><a href="<?php echo site_url("kasir/book_kue/detail/") . $dt->id; ?>" class="btn btn-primary">Detail</a></td>
                                                </tr>
                                            <?php
                                            } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>