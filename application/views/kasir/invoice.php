  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
          <div class="container-fluid">
              <div class="row mb-2">
                  <div class="col-sm-6">
                      <h1>Invoice</h1>
                  </div>
                  <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                          <li class="breadcrumb-item"><a href="#">Home</a></li>
                          <li class="breadcrumb-item active">Invoice</li>
                      </ol>
                  </div>
              </div>
          </div><!-- /.container-fluid -->
      </section>

      <section class="content">
          <div class="container-fluid">
              <div class="row" style="display:none;" id="container-invoice">
                  <div class="col-12">
                      <!-- Main content -->
                      <div class="invoice p-3 mb-3">
                          <!-- title row -->
                          <div class="row">
                              <div class="col-12">
                                  <h4>
                                      KurniaCake Bakery
                                  </h4>
                              </div>
                              <!-- /.col -->
                          </div>
                          <!-- info row -->
                          <div class="row invoice-info" style="margin-bottom:25px;">
                              <div class="col-sm-6 invoice-col">
                                  <b>Invoice </b>
                                  <p>#<?php echo $orderid; ?></p>
                                  <b>Order ID:</b>
                                  <p><?php echo $orderid; ?></p>
                                  <b>Transaction Date:</b>
                                  <p> <?php echo date("d/m/Y"); ?></p>
                                  <form id="formsubmit" action="<?php echo site_url("submit_payment"); ?>" method="POST">
                                      <b>Email Customer:</b> <br><input type="email" placeholder="Email Customer" class="form-control" id="emailcust" name="emailcust">
                                      <input type="hidden" id="orderid" name="orderid" value="<?php echo $orderid; ?>" />
                                      <input type="hidden" id="form_alamat" name="alamat" value="" />
                                      <input type="hidden" id="form_fee_shipping" name="fee_shipping" value="0" />
                                      <input type="hidden" id="form_date_delivery" name="date_delivery" />
                                      <input type="hidden" id="form_nama_penerima" name="nama_penerima" value="" />
                                      <input type="hidden" id="form_metode_pembayaran" name="metode_pembayaran" value="" />
                                  </form>
                              </div>
                              <div class="col-sm-6 invoice-col" style="text-align:right; visibility:hidden;" id="panel_delivery">
                                  <b>Alamat : </b>
                                  <p id="label_alamat">-</p>
                                  <b>Distance : </b>
                                  <p id="label_distance">-</p>
                                  <b>Nama Penerima :</b>
                                  <p id="label_nama">-</p>
                                  <b>Delivery Date:</b>
                                  <p id="label_delivery_date">-</p>
                              </div>
                              <!-- /.col -->
                          </div>
                          <!-- /.row -->

                          <!-- Table row -->
                          <div class="row">
                              <div class="col-12 table-responsive">
                                  <table class="table table-striped">
                                      <thead>
                                          <tr>
                                              <th>Qty</th>
                                              <th>Product</th>
                                              <th>Price</th>
                                              <th>Subtotal</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <?php foreach ($cart as $dt) { ?>
                                              <tr>
                                                  <td><?php echo $dt->qty; ?></td>
                                                  <td><?php echo $dt->nama_kue; ?></td>
                                                  <td>Rp. <?php echo number_format($dt->price, 0, ",", "."); ?></td>
                                                  <td>Rp. <?php echo number_format($dt->subtotal, 0, ",", "."); ?></td>
                                              </tr>
                                          <?php } ?>
                                      </tbody>
                                  </table>
                              </div>
                              <!-- /.col -->
                          </div>
                          <!-- /.row -->

                          <div class="row">
                              <!-- accepted payments column -->
                              <div class="col-6">
                              </div>
                              <!-- /.col -->
                              <div class="col-6">
                                  <div class="table-responsive">
                                      <table class="table">
                                          <tr>
                                              <th style="width:50%">Subtotal:</th>
                                              <td>Rp. <?php echo number_format($subtotal, 0, ",", "."); ?></td>
                                          </tr>
                                          <tr>
                                              <th>Shipping:</th>
                                              <td id="invoice_shipment">-</td>
                                          </tr>
                                          <tr>
                                              <th>Total:</th>
                                              <td id="label_grand_total">Rp. <?php echo number_format($subtotal, 0, ",", "."); ?></td>
                                          </tr>
                                      </table>
                                  </div>
                              </div>
                              <!-- /.col -->
                          </div>
                          <!-- /.row -->

                          <!-- this row will not appear when printing -->
                          <div class="row no-print">
                              <div class="col-12">
                                  <div onclick="doprint()" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</div>
                                  <button type="button" class="btn btn-success float-right" onclick="submitPayment()"><i class="far fa-credit-card"></i> Manual
                                      Payment
                                  </button>
                                  <button type="button" class="btn btn-primary float-right" style="margin-right:15px;" onclick="submitPaymentXendit()"><i class="far fa-credit-card"></i> Xendit
                                      Payment
                                  </button>
                              </div>
                          </div>
                      </div>
                      <!-- /.invoice -->
                  </div><!-- /.col -->
              </div><!-- /.row -->

              <div class="invoice" style="padding:50px; visibility:hidden;" id="container-type">
                  <h5 style="font-weight:bold;">Select Transaction Type</h5>
                  <div class="row">
                      <div class="col-6 btn-type">
                          <div class="container-btn" onclick="takeAway()">
                              <img src="<?php echo base_url("assets/dist/img/ico-delivery.png"); ?>" width="200px;">
                              <h5>Take Away / Delivery</h5>
                          </div>
                      </div><!-- /.col -->
                      <div class="col-6 btn-type">
                          <div class="container-btn" onclick="dineIn()">
                              <img src="<?php echo base_url("assets/dist/img/ico-takeaway.png"); ?>" width="200px;">
                              <h5>Dine in</h5>
                          </div>
                      </div>
                  </div>
              </div>

              <div class="invoice" id="container-delivery" style="visibility:hidden;">
                  <h3 style="text-align:center; font-weight:bold;margin-top:15px;">Deliver To</h3>
                  <div id='map' style='height: 700px;'></div>
                  <div class="container-map">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tanggal Kirim</label>
                          <input type="date" class="form-control" id="date_delivery">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Alamat</label>
                          <input type="text" class="form-control" id="alamat" readonly>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Distance</label>
                          <input type="text" class="form-control" id="distance" readonly value="0">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Fee Shipment</label>
                          <input type="text" class="form-control" id="fee_shipment" readonly value="0">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Nama Penerima</label>
                          <input type="text" class="form-control" id="nama_penerima" placeholder="Nama Penerima">
                      </div>
                      <div class="btn btn-primary" onclick="submitDelivery()">Submit Delivery</div>
                  </div>
              </div>
          </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <style>
      #map {
          margin: 2%;
          width: 95%;
      }

      .container-map {
          padding: 25px;
          margin-top: 25px;
      }

      .btn-type {
          text-align: center;
      }

      .container-btn {
          padding: 50px;
          border-radius: 25px;
          margin: 25px 50px;
          background-color: #F4F6F9;
          transition: all 0.5s;
      }

      .container-btn:hover {
          cursor: pointer;
          background-color: #007BFF;
          color: white;
      }

      .container-btn h5 {
          margin-top: 25px;
          font-weight: bold;
      }
  </style>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
  <link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />
  <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.7.0/mapbox-gl-geocoder.min.js'></script>
  <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.7.0/mapbox-gl-geocoder.css' type='text/css' />
  <script src='https://unpkg.com/@turf/turf/turf.min.js'></script>
  <script>
      function takeAway() {
          $("#container-type").css("display", "none");
          $("#container-delivery").css("display", "block");
      }

      function dineIn() {
          $("#container-type").css("display", "none");
          $("#container-invoice").css("display", "block");
      }

      function submitDelivery() {
          $("#container-delivery").css("display", "none");
          $("#container-invoice").css("display", "block");

          $("#invoice_shipment").html("Rp. " + $("#fee_shipment").val());
          $("#label_alamat").html($("#alamat").val());
          $("#label_distance").html($("#distance").val() + " km");
          $("#label_nama").html($("#nama_penerima").val());
          $("#label_delivery_date").html($("#date_delivery").val());
          $("#form_alamat").val($("#alamat").val());
          $("#form_fee_shipping").val($("#fee_shipment").val());
          $("#form_nama_penerima").val($("#nama_penerima").val());
          $("#form_date_delivery").val($("#date_delivery").val());
          $("#panel_delivery").css("visibility", "visible");

          var subtotal = <?php echo $subtotal; ?>;
          var grand_total = parseInt($("#fee_shipment").val()) + parseInt(subtotal);
          $("#label_grand_total").html("Rp. " + Intl.NumberFormat('de-DE').format(Math.round(grand_total)));

      }

      function doprint() {
          window.print();
      }

      function submitPayment() {
          swal({
                  title: "Selesaikan payment?",
                  text: "Pembayaran akan diselesaikan, dan akan disimpan",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
              })
              .then((willDelete) => {
                  if (willDelete) {
                      $("#form_metode_pembayaran").val("MANUAL");
                      $("#formsubmit").submit();
                  }
              });
      }

      function submitPaymentXendit() {
          $("#form_metode_pembayaran").val("XENDIT");
          $("#formsubmit").attr("target", "_blank");

          $("#formsubmit").submit();
          setTimeout(function() {
              window.location.href = '<?php echo site_url("kasir/history_transaction") . "/" . $next_id; ?>';
          }, 3000);
      }

      mapboxgl.accessToken = 'pk.eyJ1IjoiZmV3czEyMzQ1NiIsImEiOiJja3EzZTNmbGQwNXlmMm5vNWd3ajViNjNvIn0.vD5e2JaOpIQPAdQgYAsQLA';
      var map = new mapboxgl.Map({
          container: 'map', // Container ID
          style: 'mapbox://styles/mapbox/streets-v11', // Map style to use
          center: [119.42887429959961, -5.16620316406214], // Starting position [lng, lat]
          zoom: 12 // Starting zoom level
      });

      var marker = new mapboxgl.Marker() // Initialize a new marker
          .setLngLat([119.42887429959961, -5.16620316406214]) // Marker [lng, lat] coordinates
          .addTo(map); // Add the marker to the map

      var geocoder = new MapboxGeocoder({
          // Initialize the geocoder
          accessToken: mapboxgl.accessToken, // Set the access token
          mapboxgl: mapboxgl, // Set the mapbox-gl instance
          marker: true, // Do not use the default marker style
          placeholder: 'Deliver to', // Placeholder text for the search bar
      });

      // Add the geocoder to the map
      map.addControl(geocoder);



      // After the map style has loaded on the page,
      // add a source layer and default styling for a single point
      map.on('load', function() {
          map.addSource('single-point', {
              'type': 'geojson',
              'data': {
                  'type': 'FeatureCollection',
                  'features': []
              }
          });

          // Listen for the `result` event from the Geocoder // `result` event is triggered when a user makes a selection
          //  Add a marker at the result's coordinates
          geocoder.on('result', function(e) {
              map.getSource('single-point').setData(e.result.geometry);
          });
          $("#container-delivery").css("display", "none");
          $("#container-delivery").css("visibility", "visible");
          $("#container-type").css("visibility", "visible");

      });
      geocoder.on('result', function(e) {
          console.log(e.result.place_name);
          //   map.getSource('single-point').setData(e.result.geometry);
          //   console.log(e.result.geometry.coordinates[0]);
          //   console.log(e.result.geometry.coordinates[1]);

          var from = [119.42887429959961, -5.16620316406214]; //lng, lat 
          var to = [e.result.geometry.coordinates[0], e.result.geometry.coordinates[1]]; //lng, lat
          var distance = turf.distance(from, to);
          //   console.log(distance);

          var value = document.getElementById('distance');

          value.value = distance.toFixed([2]);
          $("#alamat").val(e.result.place_name);
          var fee = (distance.toFixed([2]) * <?php echo $fee_shipment_per_km->value; ?>).toFixed([0]);
          $("#fee_shipment").val(fee);

      });
  </script>