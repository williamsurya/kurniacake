  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
          <div class="container-fluid">
              <div class="row mb-2">
                  <div class="col-sm-6">
                      <h1>History Invoice</h1>
                  </div>
                  <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                          <li class="breadcrumb-item"><a href="#">Home</a></li>
                          <li class="breadcrumb-item active">History Invoice</li>
                      </ol>
                  </div>
              </div>
          </div><!-- /.container-fluid -->
      </section>

      <section class="content">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-12">
                      <!-- Main content -->
                      <div class="invoice p-3 mb-3">
                          <!-- title row -->
                          <div class="row">
                              <div class="col-12">
                                  <h4>
                                      KurniaCake Bakery
                                  </h4>
                              </div>
                              <!-- /.col -->
                          </div>
                          <!-- info row -->
                          <div class="row invoice-info" style="margin-bottom:25px;">
                              <div class="col-sm-6 invoice-col">
                                  <b>Invoice </b>
                                  <p>#<?php echo $data_header->no_inv; ?></p>
                                  <b>Order ID:</b>
                                  <p><?php echo $data_header->no_inv; ?></p>
                                  <b>Transaction Date:</b>
                                  <p> <?php echo $data_header->date; ?></p>
                                  <b>Status Payment:</b>
                                  <p> <?php echo $data_header->status == 1 ? 'Sukses' : 'Pending'; ?></p>
                                  <b <?php echo $data_header->email_customer == '' ? 'hidden' : ''; ?>>Email Customer:</b>
                                  <p><?php echo $data_header->email_customer; ?></p>
                              </div>
                              <div class="col-sm-6 invoice-col" style="text-align:right; visibility:<?php echo  !isset($data_header->alamat) ? 'hidden' : ($data_header->alamat != "" ? 'visible' : 'hidden'); ?>;">
                                  <b>Alamat </b>
                                  <p><?php echo $data_header->alamat; ?></p>
                                  <b>Nama Penerima</b>
                                  <p> <?php echo $data_header->nama_penerima; ?></p>
                                  <b>Tanggal Kirim</b>
                                  <p> <?php echo $data_header->date_delivery; ?></p>
                              </div>
                              <!-- /.col -->
                          </div>
                          <!-- /.row -->

                          <!-- Table row -->
                          <div class="row">
                              <div class="col-12 table-responsive">
                                  <table class="table table-striped">
                                      <thead>
                                          <tr>
                                              <th>Qty</th>
                                              <th>Product</th>
                                              <th>Price</th>
                                              <th>Subtotal</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <?php foreach ($data_detail as $dt) { ?>
                                              <tr>
                                                  <td><?php echo $dt->qty; ?></td>
                                                  <td><?php echo $dt->nama_kue; ?></td>
                                                  <td>Rp. <?php echo number_format($dt->price, 0, ",", "."); ?></td>
                                                  <td>Rp. <?php echo number_format($dt->subtotal, 0, ",", "."); ?></td>
                                              </tr>
                                          <?php } ?>
                                      </tbody>
                                  </table>
                              </div>
                              <!-- /.col -->
                          </div>
                          <!-- /.row -->

                          <div class="row">
                              <!-- accepted payments column -->
                              <div class="col-6">
                              </div>
                              <!-- /.col -->
                              <div class="col-6">
                                  <div class="table-responsive">
                                      <table class="table">
                                          <tr>
                                              <th style="width:50%">Subtotal:</th>
                                              <td>Rp. <?php echo number_format($data_header->grand_total, 0, ",", "."); ?></td>
                                          </tr>
                                          <tr>
                                              <th>Discount (0%)</th>
                                              <td>Rp. <?php echo number_format($data_header->discount, 0, ",", "."); ?></td>
                                          </tr>
                                          <tr>
                                              <th>Shipping:</th>
                                              <td>Rp. <?php echo number_format($data_header->fee_shipping, 0, ",", "."); ?></td>
                                          </tr>
                                          <tr>
                                              <th>Total:</th>
                                              <td>Rp. <?php echo number_format($data_header->grand_total, 0, ",", "."); ?></td>
                                          </tr>
                                      </table>
                                  </div>
                              </div>
                              <!-- /.col -->
                          </div>
                          <!-- /.row -->

                          <!-- this row will not appear when printing -->

                          <form id="form1" action="<?php echo site_url("update_status_order"); ?>" method="POST">
                              <input type="hidden" id="id_order" name="id" value="<?php echo $data_header->id; ?>" />
                              <input type="hidden" id="status_order" name="status" />
                          </form>

                          <div class="row no-print">
                              <div class="col-12">
                                  <div onclick="doprint()" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</div>
                                  <div style="margin-top:25px; <?php echo $data_header->status != 0 ? 'display:none;' : ''; ?>">
                                      <div onclick="finishOrder()" class="btn btn-success">Finish Order</div>
                                      <div onclick="cancelOrder()" class="btn btn-danger">Cancel Order</div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- /.invoice -->
                  </div><!-- /.col -->
              </div><!-- /.row -->
          </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
      function doprint() {
          window.print();
      }

      function finishOrder() {
          $("#status_order").val(1);
          $("#form1").submit();
      }

      function cancelOrder() {
          $("#status_order").val(-1);
          $("#form1").submit();
      }
  </script>