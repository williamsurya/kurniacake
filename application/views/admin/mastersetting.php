<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Setting</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit Setting</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="<?php echo site_url("/admin/master_setting/do_edit"); ?>" method="POST">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Setting</label>
                                    <select class="form-control" name="id" id="id" onchange="settingChange()">
                                        <?php foreach ($data as $dt) { ?>
                                            <option value="<?php echo $dt->id; ?>"><?php echo $dt->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Value</label>
                                    <input id="value" value="<?php echo $data[0]->value ?>" type="text" class="form-control" name="value" placeholder="Harga Jual">
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    var data = <?php echo json_encode($data); ?>;
    console.log(data);

    function settingChange() {
        var index = document.getElementById("id").selectedIndex;
        document.getElementById("value").value = data[index].value;
    }
</script>