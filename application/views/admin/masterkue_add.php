<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Kue</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add Kue</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="<?php echo site_url("/admin/master_kue/do_add"); ?>" method="POST">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Kue</label>
                                    <input type="text" class="form-control" name="nama" placeholder="Enter username">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kategori</label>
                                    <select class="form-control" name="id_kategori">
                                        <?php foreach ($kategori as $dt) { ?>
                                            <option value="<?php echo $dt->id; ?>"><?php echo $dt->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Harga Jual</label>
                                    <input type="numeric" class="form-control" name="harga_jual" placeholder="Harga Jual">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Harga Beli</label>
                                    <input type="numeric" class="form-control" name="harga_beli" placeholder="Harga Beli">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Durasi Kue Segar</label>
                                    <input type="numeric" class="form-control" name="durasi_kue_segar" placeholder="Hari">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Durasi Kue Diskon</label>
                                    <input type="numeric" class="form-control" name="durasi_kue_diskon" placeholder="Hari">
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>