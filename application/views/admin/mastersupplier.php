<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Master Outlet</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <a href="<?php echo site_url("/admin/master_supplier/add"); ?>" class="btn btn-primary">Add Outlet</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nama Outlet</th>
                                        <th>Alamat</th>
                                        <th>Telp</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data as $dt) { ?>
                                        <tr>
                                            <td><?php echo $dt->nama_outlet; ?></td>
                                            <td><?php echo $dt->alamat; ?></td>
                                            <td><?php echo $dt->telp; ?></td>
                                            <td>
                                                <a href="<?php echo site_url("admin/master_supplier/edit/") . $dt->id ?>" class="btn btn-primary">Edit</a>
                                                <a href="<?php echo site_url("admin/master_supplier/delete/") . $dt->id ?>" class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>