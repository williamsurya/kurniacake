<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Laporan</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Laporan</a></li>
            <li class="breadcrumb-item active">Penjualan</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Main row -->
      <div class="card">
        <div class="card-header">
          <h5>Laporan Penjualan</h5>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <div style="text-align:center; display:flex;">
            <div style="flex-basis:50%;padding:15px;">
              <label>Filter Laporan</label><br>
              <input id="filterTanggal" class="form-control" type="month" onchange="filterChange()" value="<?php echo isset($_GET['year']) ? $_GET['year'] . "-" . $_GET['month'] : ''; ?>" />
            </div>
            <div style="flex-basis:50%;padding:15px;">
              <label>Filter Kategori</label><br>
              <select id="filterKategori" class="form-control" onchange="filterChange()">
                <option value="all" <?php echo !isset($_GET['kategori']) || $_GET['kategori'] == 'all' ? 'selected' : '' ?>>All</option>
                <option value="dine_in" <?php echo $_GET['kategori'] == 'dine_in' ? 'selected' : '' ?>>Dine in</option>
                <option value="delivery" <?php echo $_GET['kategori'] == 'delivery' ? 'selected' : '' ?>>Delivery</option>
              </select>
            </div>
          </div>
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Date</th>
                <th>Invoice</th>
                <th>Delivery</th>
                <th>Email Customer</th>
                <th>Grand Total</th>
              </tr>
            </thead>
            <tbody>
              <?php $total = 0;
              foreach ($data as $dt) {
              ?>
                <tr>
                  <td><?php echo $dt->date; ?></td>
                  <td><?php echo $dt->no_inv; ?></td>
                  <td><?php echo $dt->fee_shipping == 0 ? 'No' : 'Yes'; ?></td>
                  <td><?php echo $dt->email_customer; ?></td>
                  <td><?php echo $dt->grand_total; ?></td>
                </tr>
              <?php $total += $dt->grand_total;
              } ?>
              <tr>
                <td colspan="4" style="text-align:center;font-weight:bold">Total</td>
                <td><?php echo $total; ?></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  function filterChange() {
    var filter = $("#filterTanggal").val();
    // console.log(filter.val());
    var param = filter.split("-");
    var kategori = $("#filterKategori").val();
    window.location.href = window.location.pathname + "?month=" + param[1] + "&year=" + param[0] + "&kategori=" + kategori;
  }
</script>