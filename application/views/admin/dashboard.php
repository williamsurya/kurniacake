<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v1</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?php echo $total_order; ?></h3>

              <p>Total Orders</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?php echo $total_cake_sold; ?></h3>

              <p>Total Cake Sold</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3>Rp. <?php echo number_format($total_profit, 0, ",", "."); ?></h3>

              <p>Total Profit</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3><?php echo $total_restok; ?></h3>

              <p>Total Need Restock</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->

      <!-- <div class="card">
        <div class="card-header">
          <h5>Grafik Penjualan Bulan ini</h5>
        </div>
        <div class="card-body">
          <canvas id="myChart"></canvas>
        </div>
      </div> -->
      <!-- Main row -->
      <div class="card">
        <div class="card-header">
          <h5>Penjualan Hari Ini</h5>
          <!-- <div class="btn btn-primary"><i class="fa fa-envelope"></i> Send</div> -->
          <div class="btn btn-primary" onclick="fnExcelReport()">
            Export as Excel
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="example2" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Date</th>
                <th>Invoice</th>
                <th>Delivery</th>
                <th>Email Customer</th>
                <th>Grand Total</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($data_penjualan_hari_ini as $dt) { ?>
                <tr>
                  <td><?php echo $dt->date; ?></td>
                  <td><?php echo $dt->no_inv; ?></td>
                  <td><?php echo $dt->fee_shipping == 0 ? 'No' : 'Yes'; ?></td>
                  <td><?php echo $dt->email_customer; ?></td>
                  <td><?php echo $dt->grand_total; ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="card">
          <div class="card-header">
            <h5>Penjualan Per Kasir</h5>
            <div class="btn btn-primary" onclick="fnExcelReport2()">
              Export as Excel
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nama Kasir</th>
                  <th>Total Kue Terjual</th>
                  <th>Total Pendapatan Kotor</th>
                  <th>Total Profit</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data_laporan_per_kasir as $dt) { ?>
                  <tr>
                    <td><?php echo $dt->name; ?></td>
                    <td><?php echo $dt->qty; ?></td>
                    <td><?php echo number_format($dt->bruto, 0, ",", "."); ?></td>
                    <td><?php echo number_format($dt->profit, 0, ",", "."); ?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>

        <div class="card">
          <div class="card-header">
            <h5>Laporan produksi hari ini</h5>
            <div class="btn btn-primary" onclick="fnExcelReport3()">
              Export as Excel
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example2" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nama Kue</th>
                  <th>Total Produksi</th>
                  <th>Total Pengeluaran</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data_laporan_pengeluaran as $dt) { ?>
                  <tr>
                    <td><?php echo $dt->nama; ?></td>
                    <td><?php echo $dt->qty; ?></td>
                    <td><?php echo number_format($dt->total_pengeluaran, 0, ",", "."); ?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.js"></script>
<script>
  var ctx = document.getElementById('myChart').getContext('2d');

  (function() {
    var data = <?php echo json_encode($data_laporan_penjualan_bulanan); ?>;
    var data_line = [];
    var label_line = [];
    data.forEach((dt) => {
      label_line.push(dt.date.substring(0, 10));
      data_line.push(dt.total);
    })
    console.log(data_line);

    var step_size = 50000;
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: label_line,
        datasets: [{
          label: 'Total Income',
          data: data_line,
          fill: false,
          borderColor: 'rgb(75, 192, 192)'
        }]
      },
      options: {
        elements: {
          line: {
            tension: 0
          }
        },
        maintainAspectRatio: true,
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              display: true
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              stepSize: step_size,
            }
          }]
        },
      }
    });
  })();

  function fnExcelReport() {
    var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange;
    var j = 0;
    tab = document.getElementById('example2'); // id of table

    for (j = 0; j < tab.rows.length; j++) {
      tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
      //tab_text=tab_text+"</tr>";
    }

    tab_text = tab_text + "</table>";
    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
    {
      txtArea1.document.open("txt/html", "replace");
      txtArea1.document.write(tab_text);
      txtArea1.document.close();
      txtArea1.focus();
      sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
    } else //other browser not tested on IE 11
      sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

    return (sa);
  }

  function fnExcelReport2() {
    var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange;
    var j = 0;
    tab = document.getElementById('example1'); // id of table

    for (j = 0; j < tab.rows.length; j++) {
      tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
      //tab_text=tab_text+"</tr>";
    }

    tab_text = tab_text + "</table>";
    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
    {
      txtArea1.document.open("txt/html", "replace");
      txtArea1.document.write(tab_text);
      txtArea1.document.close();
      txtArea1.focus();
      sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
    } else //other browser not tested on IE 11
      sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

    return (sa);
  }
  function fnExcelReport3() {
    var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange;
    var j = 0;
    tab = document.getElementById('example2'); // id of table

    for (j = 0; j < tab.rows.length; j++) {
      tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
      //tab_text=tab_text+"</tr>";
    }

    tab_text = tab_text + "</table>";
    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
    {
      txtArea1.document.open("txt/html", "replace");
      txtArea1.document.write(tab_text);
      txtArea1.document.close();
      txtArea1.focus();
      sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
    } else //other browser not tested on IE 11
      sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

    return (sa);
  }
</script>