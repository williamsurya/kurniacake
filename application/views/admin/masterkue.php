<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Master Kue</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <a href="<?php echo site_url("/admin/master_kue/add"); ?>" class="btn btn-primary">Add Kue</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Id Kategori</th>
                                        <th>Nama Kue</th>
                                        <th>Harga Beli</th>
                                        <th>Harga Jual</th>
                                        <th>Stok</th>
                                        <th>Stok Lama</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data as $dt) { ?>
                                        <tr>
                                            <td><?php echo $dt->id; ?></td>
                                            <td><?php echo $dt->id_kategori; ?></td>
                                            <td><?php echo $dt->nama; ?></td>
                                            <td><?php echo $dt->harga_beli; ?></td>
                                            <td><?php echo $dt->harga_jual; ?></td>
                                            <td><?php echo $dt->stok; ?></td>
                                            <td><?php echo $dt->stok_lama ?></td>
                                            <td>
                                                <a href="<?php echo site_url("admin/master_kue/edit/") . $dt->id ?>" class="btn btn-primary">Edit</a>
                                                <a href="<?php echo site_url("admin/master_kue/delete/") . $dt->id ?>" class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>