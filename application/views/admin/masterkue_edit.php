<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Kue</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit Kue</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="<?php echo site_url("/admin/master_kue/do_edit"); ?>" method="POST" enctype="multipart/form-data">
                            <div class="card-body">
                                <input type="hidden" name="id" value="<?php echo $data->id; ?>">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Kue</label>
                                    <input value="<?php echo $data->nama; ?>" type="text" class="form-control" name="nama" placeholder="Enter username">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kategori</label>
                                    <select class="form-control" name="id_kategori">
                                        <?php foreach ($kategori as $dt) { ?>
                                            <option value="<?php echo $dt->id; ?>" <?php echo ($dt->id == $data->id_kategori) ? "selected" : ""; ?>><?php echo $dt->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Harga Jual</label>
                                    <input value="<?php echo $data->harga_jual; ?>" type="numeric" class="form-control" name="harga_jual" placeholder="Harga Jual">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Harga Beli</label>
                                    <input value="<?php echo $data->harga_beli; ?>" type="numeric" class="form-control" name="harga_beli" placeholder="Harga Beli">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Durasi Kue Segar</label>
                                    <input value="<?php echo $data->durasi_kue_segar; ?>" type="numeric" class="form-control" name="durasi_kue_segar" placeholder="Hari">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Durasi Kue Diskon</label>
                                    <input value="<?php echo $data->durasi_kue_diskon; ?>" type="numeric" class="form-control" name="durasi_kue_diskon" placeholder="Hari">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Diskon Kue Lama (%)</label>
                                    <input value="<?php echo $data->diskon_kue_lama; ?>" type="numeric" class="form-control" name="diskon_kue_lama" placeholder="Diskon Kue Lama">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Gambar Kue</label><br>
                                    <input type="file" name="fileToUpload" id="fileToUpload">
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>