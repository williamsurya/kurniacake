<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Master User</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <a href="<?php echo site_url("/admin/master_user/add"); ?>" class="btn btn-primary">Add User</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Username</th>
                                        <th>Nama User</th>
                                        <th>Role</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data as $dt) { ?>
                                        <tr>
                                            <td><?php echo $dt->username; ?></td>
                                            <td><?php echo $dt->name; ?></td>
                                            <td>
                                                <?php
                                                if ($dt->role == 1) {
                                                    echo "Admin";
                                                } else if ($dt->role == 2) {
                                                    echo "Kasir";
                                                } else {
                                                    echo "Baker";
                                                }
                                                ?></td>
                                            <td>
                                                <a href="<?php echo site_url("admin/master_user/edit/") . $dt->id ?>" class="btn btn-primary">Edit</a>
                                                <a href="<?php echo site_url("admin/master_user/delete/") . $dt->id ?>" class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>