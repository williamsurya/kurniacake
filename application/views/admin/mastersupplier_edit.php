<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Outlet</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit Outlet</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="<?php echo site_url("/admin/master_supplier/do_edit"); ?>" method="POST">
                            <input type="hidden" name="id" value="<?php echo $data->id; ?>" />
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Outlet</label>
                                    <input type="text" class="form-control" name="nama_outlet" placeholder="Nama Outlet" value="<?php echo $data->nama_outlet; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Alamat</label>
                                    <input type="text" class="form-control" name="alamat" placeholder="Alamat" value="<?php echo $data->alamat; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Telp</label>
                                    <input type="text" class="form-control" name="telp" placeholder="Telephone" value="<?php echo $data->telp; ?>">
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>