<!DOCTYPE html>
<html>

<head>
    <style>
        body {
            max-width: 800px;
            margin: auto;
        }

        h3 {
            text-align: center;
            font-weight: bold;
            font-size: 26px;
        }

        .detail {
            margin: 15px 0px;
        }

        h5 {
            font-weight: bold;
            font-size: 16px;
            padding: 0px;
            margin: 5px 0px;
        }

        p {
            padding: 0;
            margin: 0px;
        }

        thead {
            font-weight: bold;
        }

        .nota {
            margin: auto;
            border: 1px solid black;
        }

        .nota td {
            border: 1px solid black;
        }

        .nota thead td {
            min-width: 100px;
            text-align: center;
        }

        .nota tbody td {
            padding: 5px;
        }

        .grand_total {
            margin-top: 25px;
            max-width: 800px;
        }

        .grand_total .item {
            width: 100%;
        }

    </style>
</head>

<body>
    <div>
        <h3>INVOICE KURNIACAKE</h3>
        <div class="description">
            <div class="detail">
                <h5>Invoice Number</h5>
                <p><?php echo $order_id; ?></p>
            </div>
            <div class="detail">
                <h5>Transaction Date</h5>
                <p><?php echo $transaction_date; ?></p>
            </div>
            <?php
            if ($alamat != "") {
            ?>
                <div class="detail">
                    <h5>Alamat Pengiriman</h5>
                    <p><?php echo $alamat; ?></p>
                </div>
                <div class="detail">
                    <h5>Nama Penerima</h5>
                    <p><?php echo $nama_penerima; ?></p>
                </div>
                <div class="detail">
                    <h5>Delivery Date</h5>
                    <p><?php echo $date_delivery; ?></p>
                </div>
            <?php
            }
            ?>
        </div>
        <h4 style="text-align:center">Detail Pembelian</h4>
        <table class="nota">
            <thead>
                <tr>
                    <td>Qty</td>
                    <td>Product</td>
                    <td>Price</td>
                    <td>Subtotal</td>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($cart as $dt) {
                ?>
                    <tr>
                        <td><?php echo $dt->qty; ?></td>
                        <td><?php echo $dt->nama_kue; ?></td>
                        <td>Rp. <?php echo number_format($dt->price, 0, ",", "."); ?></td>
                        <td>Rp. <?php echo number_format($dt->subtotal, 0, ",", "."); ?></td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>

        <div class="grand_total">
            <div class="item">
                <h5>Subtotal</h5>
                <p>Rp. <?php echo number_format($subtotal, 0, ",", "."); ?></p>
            </div>
            <div class="item">
                <h5>Fee Shipment</h5>
                <p>Rp. <?php echo number_format($fee_shipping, 0, ",", "."); ?></p>
            </div>
            <div class="item">
                <h5>Grand Total</h5>
                <p>Rp. <?php echo number_format($grand_total, 0, ",", "."); ?></p>
            </div>
        </div>

    </div>
</body>

</html>