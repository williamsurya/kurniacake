-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2021 at 03:19 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kurnia_cake`
--

-- --------------------------------------------------------

--
-- Table structure for table `d_invoice`
--

CREATE TABLE `d_invoice` (
  `id` int(11) NOT NULL,
  `id_header` int(11) NOT NULL,
  `id_kue` int(11) NOT NULL,
  `nama_kue` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `d_invoice`
--

INSERT INTO `d_invoice` (`id`, `id_header`, `id_kue`, `nama_kue`, `qty`, `price`, `subtotal`) VALUES
(13, 10, 3, 'Bolu Pisang', 2, 6500, 13000),
(14, 11, 6, 'Kue Lumpur', 4, 4500, 18000),
(15, 11, 5, 'Onde-Onde', 1, 5000, 5000),
(16, 12, 1, 'Kue Lemper', 1, 5000, 5000),
(17, 12, 4, 'Dadar Gulung', 1, 3500, 3500),
(18, 12, 7, 'Nastar', 1, 28500, 28500),
(19, 13, 7, 'Nastar', 3, 28500, 85500),
(20, 14, 2, 'Pukis', 2, 5000, 10000),
(21, 15, 3, 'Bolu Pisang', 2, 6500, 13000),
(22, 16, 6, 'Kue Lumpur', 2, 4500, 9000),
(23, 16, 5, 'Onde-Onde', 1, 5000, 5000),
(24, 16, 4, 'Dadar Gulung', 1, 3500, 3500),
(25, 17, 5, 'Onde-Onde', 1, 5000, 5000),
(26, 17, 6, 'Kue Lumpur', 2, 4500, 9000),
(27, 17, 8, 'Putri Salju', 1, 20000, 20000),
(28, 17, 7, 'Nastar', 1, 28500, 28500),
(29, 17, 4, 'Dadar Gulung', 1, 3500, 3500),
(30, 17, 9, 'Kue Semprit', 2, 35000, 70000),
(31, 18, 1, 'Kue Lemper', 2, 5000, 10000),
(32, 19, 4, 'Dadar Gulung', 3, 3500, 10500);

-- --------------------------------------------------------

--
-- Table structure for table `h_invoice`
--

CREATE TABLE `h_invoice` (
  `id` int(11) NOT NULL,
  `id_kasir` int(11) NOT NULL,
  `no_inv` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_delivery` date DEFAULT NULL,
  `email_customer` varchar(255) NOT NULL,
  `discount` double NOT NULL,
  `fee_shipping` int(11) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `nama_penerima` varchar(255) DEFAULT NULL,
  `grand_total` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `h_invoice`
--

INSERT INTO `h_invoice` (`id`, `id_kasir`, `no_inv`, `date`, `date_delivery`, `email_customer`, `discount`, `fee_shipping`, `alamat`, `nama_penerima`, `grand_total`, `status`) VALUES
(10, 2, 'INV30072021031140', '2021-07-30 13:12:45', '0000-00-00', '', 0, 0, '', '', 13000, 1),
(11, 2, 'INV30072021031448', '2021-07-30 13:15:09', '2021-07-31', '', 0, 28300, 'Paccerakang, 90241, Biring Kanaya, Makassar, South Sulawesi, Indonesia', 'Agus', 51300, 0),
(12, 4, 'INV30072021031631', '2021-07-30 13:16:35', '0000-00-00', '', 0, 0, '', '', 37000, 1),
(13, 4, 'INV30072021031751', '2021-07-30 13:18:11', '2021-07-30', '', 0, 15925, 'Tamalanrea, Makassar, South Sulawesi, Indonesia', 'gae', 101425, 0),
(14, 2, 'INV19082021071149', '2021-08-19 17:12:04', '0000-00-00', 'williamsurya48@gmail.com', 0, 0, '', '', 10000, 1),
(15, 2, 'INV19082021074527', '2021-08-19 17:45:37', '0000-00-00', 'williamsurya48@gmail.com', 0, 0, '', '', 13000, 1),
(16, 2, 'INV19082021074754', '2021-08-19 17:48:03', '0000-00-00', 'williamsurya48@gmail.com', 0, 0, '', '', 17500, 1),
(17, 2, 'INV19082021074901', '2021-08-19 17:49:31', '2021-08-19', 'williamsurya48@gmail.com', 0, 10350, 'Bontoala, Makassar, South Sulawesi, Indonesia', 'Agus', 146350, -1),
(18, 2, 'INV22082021082821', '2021-08-22 06:28:27', '0000-00-00', '', 0, 0, '', '', 10000, 1),
(19, 2, 'INV22082021082842', '2021-08-22 06:29:05', '2021-08-24', '', 0, 7300, 'Maccini Parang, 90144, Makassar, Makassar, South Sulawesi, Indonesia', 'Agus', 17800, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`) VALUES
(1, 'Kue Basah'),
(2, 'Kue Kering');

-- --------------------------------------------------------

--
-- Table structure for table `kue`
--

CREATE TABLE `kue` (
  `id` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kue`
--

INSERT INTO `kue` (`id`, `id_kategori`, `nama`, `harga_beli`, `harga_jual`, `stok`) VALUES
(1, 2, 'Kue Lemper', 2500, 5000, 7),
(2, 1, 'Pukis', 2500, 5000, 3),
(3, 1, 'Bolu Pisang', 3000, 6500, 0),
(4, 1, 'Dadar Gulung', 2500, 3500, 9),
(5, 1, 'Onde-Onde', 2500, 5000, 9),
(6, 1, 'Kue Lumpur', 3500, 4500, 18),
(7, 2, 'Nastar', 15000, 28500, 10),
(8, 2, 'Putri Salju', 15000, 20000, 14),
(9, 2, 'Kue Semprit', 15000, 35000, 13),
(10, 2, 'Lidah Kucing', 10000, 20000, 25),
(11, 2, 'Kue Sagu', 8000, 15000, 15);

-- --------------------------------------------------------

--
-- Table structure for table `request_stok`
--

CREATE TABLE `request_stok` (
  `id` int(11) NOT NULL,
  `id_kue` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `request_stok`
--

INSERT INTO `request_stok` (`id`, `id_kue`, `qty`, `status`) VALUES
(2, 3, 15, 1),
(3, 1, 5, 0),
(4, 1, 10, 1),
(5, 3, 15, 1),
(6, 2, 5, 1),
(7, 2, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `nama`, `value`) VALUES
(1, 'START_HOUR_PROMO', '18:30'),
(2, 'END_HOUR_PROMO', '21:00'),
(3, 'HOUR_PROMO_PERCENTAGE', '30'),
(4, 'FEE_SHIPMENT_PER_KM', '2500');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `telp` varchar(18) NOT NULL,
  `alamat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `nama`, `kategori`, `telp`, `alamat`) VALUES
(1, 'Supplier 1', 'Tepung', '081234123123', 'Jl Jalan 22'),
(2, 'Supplier 2', 'Gula', '08123443125', 'Jl Jalan 432');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role`, `name`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'admin'),
(2, 'kasir', 'c7911af3adbd12a035b289556d96470a', 2, 'kasir'),
(3, 'koki', 'c38be0f1f87d0e77a0cd2fe6941253eb', 3, 'koki'),
(4, 'kasir2', '8c86013d8ba23d9b5ade4d6463f81c45', 2, 'Kasir 2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `d_invoice`
--
ALTER TABLE `d_invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_header` (`id_header`);

--
-- Indexes for table `h_invoice`
--
ALTER TABLE `h_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kue`
--
ALTER TABLE `kue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kue_ibfk_1` (`id_kategori`);

--
-- Indexes for table `request_stok`
--
ALTER TABLE `request_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `d_invoice`
--
ALTER TABLE `d_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `h_invoice`
--
ALTER TABLE `h_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kue`
--
ALTER TABLE `kue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `request_stok`
--
ALTER TABLE `request_stok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `d_invoice`
--
ALTER TABLE `d_invoice`
  ADD CONSTRAINT `d_invoice_ibfk_1` FOREIGN KEY (`id_header`) REFERENCES `h_invoice` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kue`
--
ALTER TABLE `kue`
  ADD CONSTRAINT `kue_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
