-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2021 at 03:52 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kurnia_cake`
--
CREATE DATABASE IF NOT EXISTS `kurnia_cake` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `kurnia_cake`;

-- --------------------------------------------------------

--
-- Table structure for table `d_invoice`
--

DROP TABLE IF EXISTS `d_invoice`;
CREATE TABLE `d_invoice` (
  `id` int(11) NOT NULL,
  `id_header` int(11) NOT NULL,
  `id_kue` int(11) NOT NULL,
  `nama_kue` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `d_invoice`
--

INSERT INTO `d_invoice` (`id`, `id_header`, `id_kue`, `nama_kue`, `qty`, `price`, `subtotal`) VALUES
(3, 4, 2, 'Pukis', 2, 5000, 10000),
(4, 4, 3, 'Bolu Pisang', 2, 6500, 13000),
(5, 5, 2, 'Pukis', 2, 5000, 10000),
(6, 5, 1, 'Kue Lemper', 5, 5000, 25000),
(7, 6, 3, 'Bolu Pisang', 11, 6500, 71500),
(8, 6, 1, 'Kue Lemper', 20, 5000, 100000),
(9, 6, 2, 'Pukis', 19, 5000, 95000);

-- --------------------------------------------------------

--
-- Table structure for table `h_invoice`
--

DROP TABLE IF EXISTS `h_invoice`;
CREATE TABLE `h_invoice` (
  `id` int(11) NOT NULL,
  `no_inv` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `email_customer` varchar(255) NOT NULL,
  `discount` double NOT NULL,
  `fee_shipping` int(11) NOT NULL,
  `grand_total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `h_invoice`
--

INSERT INTO `h_invoice` (`id`, `no_inv`, `date`, `email_customer`, `discount`, `fee_shipping`, `grand_total`) VALUES
(4, 'INV12062021090251', '2021-06-12 07:03:42', 'williamsurya48@gmail.com', 0, 0, 23000),
(5, 'INV12062021090445', '2021-06-12 07:04:51', '', 0, 0, 35000),
(6, 'INV12062021091821', '2021-06-12 07:18:26', '', 0, 0, 266500);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`) VALUES
(1, 'Kue Basah'),
(2, 'Kue Kering');

-- --------------------------------------------------------

--
-- Table structure for table `kue`
--

DROP TABLE IF EXISTS `kue`;
CREATE TABLE `kue` (
  `id` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kue`
--

INSERT INTO `kue` (`id`, `id_kategori`, `nama`, `harga_beli`, `harga_jual`, `stok`) VALUES
(1, 2, 'Kue Lemper', 2500, 5000, 0),
(2, 1, 'Pukis', 2500, 5000, 0),
(3, 1, 'Bolu Pisang', 3000, 6500, 0),
(4, 1, 'Dadar Gulung', 2500, 3500, 15),
(5, 1, 'Onde-Onde', 2500, 5000, 12),
(6, 1, 'Kue Lumpur', 3500, 4500, 26),
(7, 2, 'Nastar', 15000, 28500, 15),
(8, 2, 'Putri Salju', 15000, 20000, 15),
(9, 2, 'Kue Semprit', 15000, 35000, 15),
(10, 2, 'Lidah Kucing', 10000, 20000, 25),
(11, 2, 'Kue Sagu', 8000, 15000, 15);

-- --------------------------------------------------------

--
-- Table structure for table `request_stok`
--

DROP TABLE IF EXISTS `request_stok`;
CREATE TABLE `request_stok` (
  `id` int(11) NOT NULL,
  `id_kue` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `request_stok`
--

INSERT INTO `request_stok` (`id`, `id_kue`, `qty`, `status`) VALUES
(2, 3, 15, 1),
(3, 1, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `nama`, `value`) VALUES
(1, 'START_HOUR_PROMO', '18:30'),
(2, 'END_HOUR_PROMO', '21:00'),
(3, 'HOUR_PROMO_PERCENTAGE', '30');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `telp` varchar(18) NOT NULL,
  `alamat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `nama`, `kategori`, `telp`, `alamat`) VALUES
(1, 'Supplier 1', 'Tepung', '081234123123', 'Jl Jalan 22'),
(2, 'Supplier 2', 'Gula', '08123443125', 'Jl Jalan 432');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role`, `name`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'admin'),
(2, 'kasir', 'c7911af3adbd12a035b289556d96470a', 2, 'kasir');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `d_invoice`
--
ALTER TABLE `d_invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_header` (`id_header`);

--
-- Indexes for table `h_invoice`
--
ALTER TABLE `h_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kue`
--
ALTER TABLE `kue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kue_ibfk_1` (`id_kategori`);

--
-- Indexes for table `request_stok`
--
ALTER TABLE `request_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `d_invoice`
--
ALTER TABLE `d_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `h_invoice`
--
ALTER TABLE `h_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kue`
--
ALTER TABLE `kue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `request_stok`
--
ALTER TABLE `request_stok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `d_invoice`
--
ALTER TABLE `d_invoice`
  ADD CONSTRAINT `d_invoice_ibfk_1` FOREIGN KEY (`id_header`) REFERENCES `h_invoice` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kue`
--
ALTER TABLE `kue`
  ADD CONSTRAINT `kue_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
